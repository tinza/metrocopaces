# Basics
## C# ##
- IEnumerable / ICollection / List etc.
- async / await  
Most of the I/O functions in windows store app are written in a async style. So make sure to understand how the execution flow works.

- LINQ  
Key words: delayed execution.

- Nullable type  
Differ value types and reference types in C#. Value types like `int` cannot be `null`, so if it has to be, use the type `int?` instead.

- Event & delegate  
Read about the event subscription mechanism.

- Reflection  
Reflection is a great way to obtain meta data, such as the name of a property or other attributes.
 
- Property vs. Member Variable  
Rule of thumb: use property at any time possible. Get familiar with the accessors (Getter and Setter), control the access to be read-only at the necessary time.

## Tooling
- LinqPad  
Good for testing small chunk of code / view data in the database
- Visual Studio 2013 Update 4  
The minimum version required for VS.
- PostMan
Test REST APi