# CityCountyService
## Project Summary
The data backend. It is a Azure mobile service project. The project interacts with the database including the following tables:

- `Place`
- `Road`
- `Segment`
- `RoadSurvey`
- `SegmentSurvey`
- `SurveyPhoto`

Note all the classes are derived from `EntityData` (this is required by Azure Mobile Service), so by default the primary key is `Id`, which is a property of the `EntityData` class. All the foreign keys are declared as property attribute. 

Each table corresponds to a class in the "DataObject" folder, and a controller in the "Controllers" folder. The detailed mapping mechanism please refer the following sites:
- [Azure Mobile Service](http://azure.microsoft.com/en-us/documentation/services/mobile-services/)
- [Entity Framework](http://msdn.microsoft.com/en-us/data/ef.aspx)

## Notes
The `CopacesCcDbContext` class is the class that works with the underlying database. One can set the properties of individual tables / columns here. And whenever a new table is added, a `DbSet` property should be declared here.

The `WebApiConfig` class contains the initializer for the database. The initializer is used when the database schema is empty or has been cleared. I hosted the initial data source (XML files) in BitBucket, and then seed them to the database in the `Seed` function of the initializer.

The Utility folder contains several utility classes that I wrote for the convenience of coding, including tools for relating a `Road` with `Place`, or a `Segment` with `Road`. 

# Copaces.CityCounty.Website
## Project Summary
This is the website used for reviewing, editing, and reporting survey data collected. The base framework is Asp.Net MVC 5.

## Notes
- Database
The website doesn't store survey data or road/place/segment data, all of which will be pulled from the backend survey. The website's database only contains the user data. Each user has a property of `Place`; and each user can only view data of the `Place` that he/she belongs to.

- The `ApiHelper` class
The website communicate with the backend service via RESTFul api. And the `ApiHelper` utility class handles most of the communication. To pull data from the server, HttpGet request would be used. And to push data to the server, HttpPost request would be used. 

- Database Migration
Because the database can be changed over time, it is important to keep track of the database changes. Please refer to the [Entity Framework Database Migration](http://msdn.microsoft.com/en-us/data/dn579398.aspx) for the detailed procedure. 

# Copaces.CityCounty.Website.Test
Unit test project for the website.
