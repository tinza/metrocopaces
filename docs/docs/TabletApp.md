# MetroCOPACESGrid
## Structure
Each `.xaml` file corresponds to each page in the app. And each `.xaml` has a corresponding `.xaml.cs` file containing the logic code (control callbacks, data loading methods etc.) for this class. And they are documented independently.

## Basics
### Page entry point
Each page has a `navigationHelper_LoadState` function, which is the entry point of the page. All the data loading should be completed within this function. Parameter can be passed via this function to allow data transfer when navigating back and forth among pages. 

### The Model-View-ViewModel (MVVM) Pattern
Regarding the data loading process, please read about the MVVM pattern that is normally used for Win store app development. Basically, `Models` include all the data objects (classes, structures etc), and each page has it's own `View` (The xaml makeup) and `ViewModel` (for data manipulation):

- The model layer includes all the code that implements the core app logic and defines the types required to model the app domain. This layer is completely independent of the view and view model layers.
- The view layer defines the UI using declarative markup. Data binding markup defines the connection between specific UI components and various view model (and sometimes model) members.
- The view model layer provides data binding targets for the view. In many cases, the view model exposes the model directly, or provides members that wrap specific model members. The view model can also define members for keeping track of data that is relevant to the UI but not to the model, such as the display order of a list of items.

For example, the `RoadSelectionPage` has a `ViewModel` class called `RoadSelectionPageViewModel`. The `RoadSelectionPageViewModel` class contains the following properties:
- `AllRoads`: all the roads available in the local database
- `CurRoad`: the currently selected `Road` object from the list
- `Roads`: the current collection of roads to be shown to the user. It could be different from the `AllRoads` because the user might have added some filters, such as "surveyed after 11/2/2014" or "has unfinished survey" or "start with 'T'".
- `Segments`: the segments that the `CurRoad` contains. It is used to be shown on the right side of the screen.

### Data-binding
Win 8.1 app uses the data-binding mechanism to display a data structure on UI. Please refer to the [guideline](http://msdn.microsoft.com/en-us/library/windows/apps/xaml/hh758320.aspx) for further information. Make sure to understand the `NotifyPropertyChanged` event and its associated interface. The use of `ObservableDictionary` is also because of the databinding.

### Page Navigation
Please refer to the official documents for page navigation and understand how frame manages the page navigation history. The `Frame.NavigateTo` is the mostly used function for navigation, and depends on the landing page, sometimes a parameters is required to be passed along with it.

### Caching
The default caching for each page is set as enabled. In this way, the user wouldn't lose input state if going forward from the current page and come back later. But the behavior is configurable in the con

### UserControl
In many places of the same style needs to be applied to a control. For example, all the distress input field should have the same font size. To reduce repeat ourselves, we can derive custom controls that inherit from the basic controls and customize the properties there.

Code example:
```csharp
public sealed class SegmentSurveyTextBox : TextBox {
    public SegmentSurveyTextBox()
    {
        DefaultStyleKey = typeof(TextBox);
		
		// Define customized style here
        FontFamily = new FontFamily("Segoe UI");
        FontSize = 15;
        Margin = new Thickness(0, 0, 10, 5);

        LostFocus += TextBox_OnLostFocus;
        DataContextChanged += TextBox_OnDataContextChanged;
        Loaded += TextBox_OnLoaded;

        IsDataValid = true;
    }
}
```

### The Common folder
Contains the generated utility classes that are rarely modified. 

### The Sync Mechanics
The sync is done using the offline support provided by azure mobile service. The detailed mechanics can be viewed in the [blog post](http://blogs.msdn.com/b/carlosfigueira/archive/2014/04/07/deep-dive-on-the-offline-support-in-the-azure-mobile-service-managed-client-sdk.aspx) and this [tutorial](http://azure.microsoft.com/en-us/documentation/articles/mobile-services-windows-store-dotnet-get-started-offline-data/). 

### Explanation about some particular code snippets
#### SyncHelper class
This static class contains all the low level data manipulation. Most of the operations are done via Linq. And understanding the linq query evaluation process is essential for this part.

#### App.xaml
This file contains some global values like "AppName". The `App.xaml.cs` decides the first page to load in this code snippet:
```csharp
if (rootFrame.Content == null)
{
    // When the navigation stack isn't restored navigate to the first page,
    // configuring the new page by passing required information as a navigation
    // parameter
    rootFrame.Navigate(typeof(PlaceSelectPage), e.Arguments);
}
// Ensure the current window is active
Window.Current.Activate();
```
In this case the `PlaceSelectPage` is the first page to load when the user opens the app.

#### MainPage.xaml
This file contains the two buttons: Start New Survey / Sync Data.
The data are loaded from a json file, which is kinda tricky. Otherwise there are nothing complex here.

#### RoadSelectionPage.xaml
This page is to select road for survey on. The main components are:

- Road list
- Search area
- Selected road info panel
- Segment List that the selected road contains
- Bottom appbar to start the survey (navigation to other page) 

# GDOTPACESLib
COPACES rating calculation library. Normally no need to touch.

# GDOTPACESLibTest
Unit test project for the `GDOTPACESLib` library. Normally no need to touch.
