﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebManagement.Models;

namespace WebManagement.Controllers
{
    public class RoadSurveysController : Controller
    {
        private WebManagementDbContext db = new WebManagementDbContext();

        // GET: RoadSurveys
        public async Task<ActionResult> Index()
        {
            var roadSurveys = db.RoadSurveys.Include(r => r.Place);
            return View(await roadSurveys.ToListAsync());
        }

        // GET: RoadSurveys/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoadSurvey roadSurvey = await db.RoadSurveys.FindAsync(id);
            if (roadSurvey == null)
            {
                return HttpNotFound();
            }
            return View(roadSurvey);
        }

        // GET: RoadSurveys/Create
        public ActionResult Create()
        {
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name");
            return View();
        }

        // POST: RoadSurveys/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,BleedAvg,BleedDeduct,BleedLevel,BlockAvg,BlockDeduct,BlockLevel,CorrugAvg,CorrugDeduct,CorrugLevel,Date,EdgeAvg,EdgeDeduct,EdgeLevel,FinishDate,IsFinished,LoadLevel1Avg,LoadLevel1Deduct,LoadLevel2Avg,LoadLevel2Deduct,LoadLevel3Avg,LoadLevel3Deduct,LoadLevel4Avg,LoadLevel4Deduct,LossAvg,LossDeduct,LossLevel,PatchAvg,PatchDeduct,PlaceId,ProjectRating,RavelAvg,RavelDeduct,RavelLevel,ReflectAvg,ReflectDeduct,ReflectLevel,RoadId,RutAvg,RutDeduct,SlopeAvg,SlopeDeduct")] RoadSurvey roadSurvey)
        {
            if (ModelState.IsValid)
            {
                db.RoadSurveys.Add(roadSurvey);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", roadSurvey.PlaceId);
            return View(roadSurvey);
        }

        // GET: RoadSurveys/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoadSurvey roadSurvey = await db.RoadSurveys.FindAsync(id);
            if (roadSurvey == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", roadSurvey.PlaceId);
            return View(roadSurvey);
        }

        // POST: RoadSurveys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,BleedAvg,BleedDeduct,BleedLevel,BlockAvg,BlockDeduct,BlockLevel,CorrugAvg,CorrugDeduct,CorrugLevel,Date,EdgeAvg,EdgeDeduct,EdgeLevel,FinishDate,IsFinished,LoadLevel1Avg,LoadLevel1Deduct,LoadLevel2Avg,LoadLevel2Deduct,LoadLevel3Avg,LoadLevel3Deduct,LoadLevel4Avg,LoadLevel4Deduct,LossAvg,LossDeduct,LossLevel,PatchAvg,PatchDeduct,PlaceId,ProjectRating,RavelAvg,RavelDeduct,RavelLevel,ReflectAvg,ReflectDeduct,ReflectLevel,RoadId,RutAvg,RutDeduct,SlopeAvg,SlopeDeduct")] RoadSurvey roadSurvey)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roadSurvey).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", roadSurvey.PlaceId);
            return View(roadSurvey);
        }

        // GET: RoadSurveys/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoadSurvey roadSurvey = await db.RoadSurveys.FindAsync(id);
            if (roadSurvey == null)
            {
                return HttpNotFound();
            }
            return View(roadSurvey);
        }

        // POST: RoadSurveys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            RoadSurvey roadSurvey = await db.RoadSurveys.FindAsync(id);
            db.RoadSurveys.Remove(roadSurvey);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
