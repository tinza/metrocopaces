﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebManagement.Models;

namespace WebManagement.Controllers
{
    public class SegmentSurveysController : Controller
    {
        private WebManagementDbContext db = new WebManagementDbContext();
        private ApplicationDbContext userDb = new ApplicationDbContext();

        // GET: SegmentSurveys
        public async Task<ActionResult> Index()
        {
            var list = await db.SegmentSurveys
                .OrderByDescending(ss => ss.SegSurveyDate).ToListAsync();

            if (!User.Identity.IsAuthenticated)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            var userPlaceId = userDb.Users.First(u => u.UserName == User.Identity.Name).PlaceId;

            return View(await db.SegmentSurveys.Where(ss => ss.PlaceId == userPlaceId).ToListAsync());
        }

        // GET: SegmentSurveys/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SegmentSurvey segmentSurvey = await db.SegmentSurveys.FindAsync(id);
            if (segmentSurvey == null)
            {
                return HttpNotFound();
            }
            return View(segmentSurvey);
        }

        // GET: SegmentSurveys/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SegmentSurveys/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,BleedLevel,BleedPercentage,BlockLevel,BlockPercentage,CopacesRating,CorrugLevel,CorrugPercentage,CrossSlopeLeft,CrossSlopeRight,EdgeLevel,EdgePercentage,InventId,IsWindshieldSurvey,LaneDirection,LaneNum,LoadLevel1,LoadLevel2,LoadLevel3,LoadLevel4,LossPavLevel,LossPavPercentage,PatchPotholeNum,PlaceId,Rater,RavelLevel,RavelPercentage,ReflectLength,ReflectLevel,ReflectNum,Remarks,RoadId,RoadSurveyDate,RoadSurveyId,RutInWp,RutOutWp,SampleLocation,SegSurveyDate,SequenceId,TreatmentMethod,TreatmentYear,WindshieldScore")] SegmentSurvey segmentSurvey)
        {
            if (ModelState.IsValid)
            {
                db.SegmentSurveys.Add(segmentSurvey);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(segmentSurvey);
        }

        // GET: SegmentSurveys/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SegmentSurvey segmentSurvey = await db.SegmentSurveys.FindAsync(id);
            if (segmentSurvey == null)
            {
                return HttpNotFound();
            }
            return View(segmentSurvey);
        }

        // POST: SegmentSurveys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,BleedLevel,BleedPercentage,BlockLevel,BlockPercentage,CopacesRating,CorrugLevel,CorrugPercentage,CrossSlopeLeft,CrossSlopeRight,EdgeLevel,EdgePercentage,InventId,IsWindshieldSurvey,LaneDirection,LaneNum,LoadLevel1,LoadLevel2,LoadLevel3,LoadLevel4,LossPavLevel,LossPavPercentage,PatchPotholeNum,PlaceId,Rater,RavelLevel,RavelPercentage,ReflectLength,ReflectLevel,ReflectNum,Remarks,RoadId,RoadSurveyDate,RoadSurveyId,RutInWp,RutOutWp,SampleLocation,SegSurveyDate,SequenceId,TreatmentMethod,TreatmentYear,WindshieldScore")] SegmentSurvey segmentSurvey)
        {
            if (ModelState.IsValid)
            {
                db.Entry(segmentSurvey).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(segmentSurvey);
        }

        // GET: SegmentSurveys/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SegmentSurvey segmentSurvey = await db.SegmentSurveys.FindAsync(id);
            if (segmentSurvey == null)
            {
                return HttpNotFound();
            }
            return View(segmentSurvey);
        }

        // POST: SegmentSurveys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            SegmentSurvey segmentSurvey = await db.SegmentSurveys.FindAsync(id);
            db.SegmentSurveys.Remove(segmentSurvey);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: SegmentSurveys/Search
        public async Task<ActionResult> Search(SegmentSuveySearchViewModel searchViewModel)
        {
            if (User.Identity.IsAuthenticated)
            {
                var userPlaceId = userDb.Users.First(u => u.UserName == User.Identity.Name).PlaceId;

                var roads = db.Roads.Where(r => r.PlaceId == userPlaceId).ToList();
                roads.Insert(0, new Road { Id = null, Name = string.Empty });
                ViewBag.RoadList = new SelectList(roads, "Id", "Name", String.Empty);

                if (!searchViewModel.IsEmptyQuery)
                    searchViewModel.Result = await db.SegmentSurveys
                        .Where(ss => ss.PlaceId == userPlaceId)
                        .Where(ss => !searchViewModel.StartDate.HasValue || ss.SegSurveyDate >= searchViewModel.StartDate)
                        .Where(ss => !searchViewModel.EndDate.HasValue || ss.SegSurveyDate <= searchViewModel.EndDate)
                        .Where(ss => String.IsNullOrEmpty(searchViewModel.RoadId) || ss.RoadId == searchViewModel.RoadId)
                        .Where(ss => !searchViewModel.MaxRating.HasValue || ss.CopacesRating <= searchViewModel.MaxRating)
                        .OrderByDescending(ss => ss.SegSurveyDate).ToListAsync();
            }

            return View(searchViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
