﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebManagement.Models;

namespace WebManagement.Controllers
{
    public class SurveyPhotoesController : Controller
    {
        private WebManagementDbContext db = new WebManagementDbContext();

        // GET: SurveyPhotoes
        public async Task<ActionResult> Index()
        {
            return View(await db.SurveyPhotoes.ToListAsync());
        }

        // GET: SurveyPhotoes/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SurveyPhoto surveyPhoto = await db.SurveyPhotoes.FindAsync(id);
            if (surveyPhoto == null)
            {
                return HttpNotFound();
            }
            return View(surveyPhoto);
        }

        // GET: SurveyPhotoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SurveyPhotoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,SurveyId,resourceName,imageUri,Latitude,Longitude,TimeTaken")] SurveyPhoto surveyPhoto)
        {
            if (ModelState.IsValid)
            {
                db.SurveyPhotoes.Add(surveyPhoto);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(surveyPhoto);
        }

        // GET: SurveyPhotoes/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SurveyPhoto surveyPhoto = await db.SurveyPhotoes.FindAsync(id);
            if (surveyPhoto == null)
            {
                return HttpNotFound();
            }
            return View(surveyPhoto);
        }

        // POST: SurveyPhotoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,SurveyId,resourceName,imageUri,Latitude,Longitude,TimeTaken")] SurveyPhoto surveyPhoto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(surveyPhoto).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(surveyPhoto);
        }

        // GET: SurveyPhotoes/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SurveyPhoto surveyPhoto = await db.SurveyPhotoes.FindAsync(id);
            if (surveyPhoto == null)
            {
                return HttpNotFound();
            }
            return View(surveyPhoto);
        }

        // POST: SurveyPhotoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            SurveyPhoto surveyPhoto = await db.SurveyPhotoes.FindAsync(id);
            db.SurveyPhotoes.Remove(surveyPhoto);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
