﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WebManagement.Models;
using PagedList;

namespace WebManagement.Controllers
{
    public class SegmentsController : Controller
    {
        private WebManagementDbContext db = new WebManagementDbContext();
        private ApplicationDbContext userDb = new ApplicationDbContext();

        // GET: Segments
        public async Task<ActionResult> Index(int? pageNum, bool? surveyedSegOnly)
        {
            if (User.Identity.IsAuthenticated == false)
                return View();

            var placeId = new ApplicationDbContext().Users
                .First(u => u.UserName == User.Identity.Name).PlaceId;

            var segments = await db.Segments
                .Where(s => s.PlaceId == placeId)
                .Include(s => s.SegmentSurveys)
                .Where(s => !surveyedSegOnly.HasValue || s.SegmentSurveys != null)
                .OrderBy(s => s.InventId)
                .ToListAsync();

            var pageNumOrDefault = pageNum ?? 1;
            const int countPerPage = 50;

            return View(segments.ToPagedList(pageNumOrDefault, countPerPage));
        }

        // GET: Segments/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Segment segment = await db.Segments.FindAsync(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            return View(segment);
        }

        // GET: Segments/Create
        public ActionResult Create()
        {
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name");
            ViewBag.RoadId = new SelectList(db.Roads, "Id", "District");
            return View();
        }

        // POST: Segments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(
                Include =
                    "Id,From,HasCurbOrGutter,InventId,LaneDirection,LaneNum,Length,PlaceId,Remarks,RoadId,SampleLocation,SequenceId,To"
                )] Segment segment)
        {
            if (ModelState.IsValid)
            {
                db.Segments.Add(segment);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", segment.PlaceId);
            ViewBag.RoadId = new SelectList(db.Roads, "Id", "District", segment.RoadId);
            return View(segment);
        }

        // GET: Segments/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Segment segment = await db.Segments.FindAsync(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", segment.PlaceId);
            ViewBag.RoadId = new SelectList(db.Roads, "Id", "District", segment.RoadId);
            return View(segment);
        }

        // POST: Segments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(
                Include =
                    "Id,From,HasCurbOrGutter,InventId,LaneDirection,LaneNum,Length,PlaceId,Remarks,RoadId,SampleLocation,SequenceId,To"
                )] Segment segment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(segment).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", segment.PlaceId);
            ViewBag.RoadId = new SelectList(db.Roads, "Id", "District", segment.RoadId);
            return View(segment);
        }

        // GET: Segments/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Segment segment = await db.Segments.FindAsync(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            return View(segment);
        }

        // POST: Segments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Segment segment = await db.Segments.FindAsync(id);
            db.Segments.Remove(segment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: Segment/History/5
        public async Task<ActionResult> History(int? id)
        {
            var list = await db.SegmentSurveys
                .Where(ss => ss.InventId == id)
                .Select(ss => new {SurveyDate = ss.SegSurveyDate, Rating = ss.CopacesRating})
                .OrderBy(ss => ss.SurveyDate)
                .ToListAsync();

            var labelList = list.Select(e => e.SurveyDate).ToList();
            var dataList = list.Select(e => e.Rating).ToList();
            var chartData = new {
                labels = labelList, 
                datasets = new[]
                {
                    new
                    {
                        label = "Rating History",
                        fillColor = "rgba(220,220,220,0.2)",
                        strokeColor = "rgba(220,220,220,1)",
                        pointColor = "rgba(220,220,220,1)",
                        pointStrokeColor = "#fff",
                        pointHighlightFill = "#fff",
                        pointHighlightStroke = "rgba(220,220,220,1)",
                        data = dataList
                    }
                }
            };
            return View(chartData);
        }
    }
}
    