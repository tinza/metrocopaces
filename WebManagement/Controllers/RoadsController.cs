﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebManagement.Models;
using PagedList;

namespace WebManagement.Controllers
{
    public class RoadsController : Controller
    {
        private readonly WebManagementDbContext db = new WebManagementDbContext();

        // GET: Roads/Create
        public ActionResult Create()
        {
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name");
            return View();
        }

        // POST: Roads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(
                Include =
                    "Id,BridgeNum,BridgeWidth,CulvertAndPipeNum,District,From,FunctionalClass,HasUnfinishedSurvey,Jurisdiction,LaneNum,LastSurveyDate,Name,PavMarkingCondition,PavWidthMax,PavWidthMin,PavWidthTypical,PlaceId,PlaceType,Remarks,ShoulderWidthMax,ShoulderWidthMin,ShoulderWidthTypical,SurfaceType,To,UnpavedShoulderWidth"
                )] Road road)
        {
            if (ModelState.IsValid)
            {
                db.Roads.Add(road);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", road.PlaceId);
            return View(road);
        }

        // GET: Roads/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var road = await db.Roads.FindAsync(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // POST: Roads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            var road = await db.Roads.FindAsync(id);
            db.Roads.Remove(road);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Roads/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var road = await db.Roads.FindAsync(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // GET: Roads/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var road = await db.Roads.FindAsync(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", road.PlaceId);
            return View(road);
        }

        // POST: Roads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(
                Include =
                    "Id,BridgeNum,BridgeWidth,CulvertAndPipeNum,District,From,FunctionalClass,HasUnfinishedSurvey,Jurisdiction,LaneNum,LastSurveyDate,Name,PavMarkingCondition,PavWidthMax,PavWidthMin,PavWidthTypical,PlaceId,PlaceType,Remarks,ShoulderWidthMax,ShoulderWidthMin,ShoulderWidthTypical,SurfaceType,To,UnpavedShoulderWidth"
                )] Road road)
        {
            if (ModelState.IsValid)
            {
                db.Entry(road).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PlaceId = new SelectList(db.Places, "Id", "Name", road.PlaceId);
            return View(road);
        }

        // GET: Roads
        public async Task<ActionResult> Index(int? pageNum)
        {
            if (User.Identity.IsAuthenticated == false)
                return View();

            var userName = User.Identity.Name;
            var placeId = new ApplicationDbContext().Users
                .First(u => u.UserName == userName).PlaceId;
            var roads = db.Roads.Where(r => r.PlaceId == placeId);
            var list = await roads.ToListAsync();
            const int countPerPage = 50;
            var pageNumOrDefault = pageNum ?? 1;
            return View(list.ToPagedList(pageNumOrDefault, countPerPage));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}