﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebManagement.Models
{
    public class Place
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public int? CountyNo { get; set; }
        public int? District { get; set; }
    }
}