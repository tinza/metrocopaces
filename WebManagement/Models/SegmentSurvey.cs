﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebManagement.Models
{
    public class SegmentSurvey
    {
        public string Id { get; set; }

        public int? BleedLevel { get; set; }

        public double? BleedPercentage { get; set; }

        public int? BlockLevel { get; set; }

        public double? BlockPercentage { get; set; }

        [Display(Name = "COPACES Rating")]
        public double? CopacesRating { get; set; }

        public int? CorrugLevel { get; set; }

        public double? CorrugPercentage { get; set; }

        public double? CrossSlopeLeft { get; set; }

        public double? CrossSlopeRight { get; set; }

        public int? EdgeLevel { get; set; }

        public double? EdgePercentage { get; set; }

        [Display(Name = "Invent Id")]
        public int InventId { get; set; }

        [Display(Name = "Is Windshield Survey")]
        public bool IsWindshieldSurvey { get; set; }

        [Display(Name = "Lane Direction")]
        public String LaneDirection { get; set; }

        [Display(Name = "Lane Number")]
        public String LaneNum { get; set; }

        public double? LoadLevel1 { get; set; }

        public double? LoadLevel2 { get; set; }

        public double? LoadLevel3 { get; set; }

        public double? LoadLevel4 { get; set; }

        public int? LossPavLevel { get; set; }

        public double? LossPavPercentage { get; set; }

        public int? PatchPotholeNum { get; set; }

        public String PlaceId { get; set; }

        public String Rater { get; set; }

        public int? RavelLevel { get; set; }

        public double? RavelPercentage { get; set; }

        public double? ReflectLength { get; set; }

        public int? ReflectLevel { get; set; }

        public double? ReflectNum { get; set; }

        public String Remarks { get; set; }

        [Display(Name = "Road Id")]
        public String RoadId { get; set; }

        [Display(Name = "Road Survey Date")]
        public DateTime? RoadSurveyDate { get; set; }

        [Display(Name = "Road Survey Id")]
        public String RoadSurveyId { get; set; }

        public double? RutInWp { get; set; }

        public double? RutOutWp { get; set; }

        public String SampleLocation { get; set; }

        [Display(Name = "Segment Survey Date")]
        public DateTime? SegSurveyDate { get; set; }

        [Display(Name = "Seq Id")]
        public int? SequenceId { get; set; }

        public String TreatmentMethod { get; set; }

        public String TreatmentYear { get; set; }

        [Display(Name = "Windshield Score")]
        public double? WindshieldScore { get; set; }
    }
}