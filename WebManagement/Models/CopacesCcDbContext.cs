﻿using System.Data.Entity;

namespace WebManagement.Models
{
    public class CopacesCcDbContext : DbContext
    {
        public CopacesCcDbContext() : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("copacescc");
        }

        public DbSet<Place> Places { get; set; }

        public DbSet<Road> Roads { get; set; }

        public DbSet<Segment> Segments { get; set; }

        public DbSet<SegmentSurvey> SegmentSurveys { get; set; }

        public DbSet<RoadSurvey> RoadSurveys { get; set; }

        public DbSet<SurveyPhoto> SurveyPhotoes { get; set; }
    }
}