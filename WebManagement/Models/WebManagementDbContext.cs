﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace WebManagement.Models
{
    public class WebManagementDbContext : DbContext
    {
        private const string ConnectionStringName = "Name=DefaultConnection";

        public WebManagementDbContext()
            : base(ConnectionStringName)
        {
        }

        public DbSet<Place> Places { get; set; }
        public DbSet<Road> Roads { get; set; }
        public DbSet<Segment> Segments { get; set; }
        public DbSet<RoadSurvey> RoadSurveys { get; set; }
        public DbSet<SegmentSurvey> SegmentSurveys { get; set; }
        public DbSet<SurveyPhoto> SurveyPhotoes { get; set; }
    }
}