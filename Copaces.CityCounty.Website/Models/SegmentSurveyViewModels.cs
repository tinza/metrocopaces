﻿using System;
using System.Collections.Generic;
using Copaces.CityCounty.Website.Models.DataObjects;

namespace Copaces.CityCounty.Website.Models
{
    public class SegmentSurveyIndexViewModel
    {
        public IEnumerable<SegmentSurvey> SegmentSurveys { get; set; }
        public string SearchRoadId { get; set; }
    }

    public class SegmentSurveyDetailViewModel
    {
        public SegmentSurvey SegmentSurvey { get; set; }
        public List<SurveyPhoto> SurveyPhotos { get; set; }
    }

    public class SegmentSuveySearchViewModel
    {
        public IEnumerable<SegmentSurvey> Result { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String RoadId { get; set; }
        public int? MaxRating { get; set; }

        public bool IsEmptyQuery {
            get
            {
                return !StartDate.HasValue && !EndDate.HasValue && String.IsNullOrEmpty(RoadId) && !MaxRating.HasValue;
            }
        }
    }
}