﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Copaces.CityCounty.Website.Models.DataObjects;

namespace Copaces.CityCounty.Website.Models
{
    public class SegmentSearchViewModel
    {
        public IEnumerable<Segment> Result { get; set; }
        public String From { get; set; }
        public String To { get; set; }
        public String RoadId { get; set; }
        public int? MaxRating { get; set; }

        public bool IsEmptyQuery
        {
            get { return From == null && To == null && RoadId == null && MaxRating == null; }
        }
    }
}