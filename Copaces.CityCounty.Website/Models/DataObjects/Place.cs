﻿using System;

namespace Copaces.CityCounty.Website.Models.DataObjects
{
    public class Place
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public int? CountyNo { get; set; }
        public int? District { get; set; }
    }
}