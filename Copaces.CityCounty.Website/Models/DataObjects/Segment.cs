﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Copaces.CityCounty.Website.Models.DataObjects
{
    public class Segment
    {
        public string Id { get; set; }

        public String From { get; set; }

        public String HasCurbOrGutter { get; set; }

        [Key, Display(Name = "Invent Id")]
        public int InventId { get; set; }

        [Display(Name = "Lane Direction")]
        public String LaneDirection { get; set; }

        [Display(Name = "Lane Number")]
        public int? LaneNum { get; set; }

        public double? Length { get; set; }

        public Place Place { get; set; }

        public String PlaceId { get; set; }

        public String Remarks { get; set; }

        public Road Road { get; set; }

        public String RoadId { get; set; }

        [Display(Name = "Sample Location")]
        public String SampleLocation { get; set; }

        [Display(Name = "Seq Id")]
        public int? SequenceId { get; set; }

        public String To { get; set; }

        public List<SegmentSurvey> SegmentSurveys { get; set; } 
    }
}