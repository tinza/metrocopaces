﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Copaces.CityCounty.Website.Controllers
{
    public class SegmentSurveysController : Controller
    {
        /// <summary>
        /// Application DB context
        /// </summary>
        ApplicationDbContext ApplicationDbContext { get; set; }

        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        UserManager<ApplicationUser> UserManager { get; set; }

        private ApplicationUser CurrentUser {
            get {
                return User.Identity.IsAuthenticated ? UserManager.FindById(User.Identity.GetUserId()) : null;
            }
        }

        public SegmentSurveysController()
        {
            ApplicationDbContext = new ApplicationDbContext();
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(this.ApplicationDbContext));
        }

        // GET: SegmentSurveys
        public async Task<ActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var list = await ApiHelper.GetSegmentSurveysByPlace(new Place() {Id = CurrentUser.PlaceId});

            return View(list);
        }

        // GET: SegmentSurveys/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SegmentSurvey segmentSurvey = await ApiHelper.GetSegmentSurveyById(id);
            var surveyPhotos = await ApiHelper.GetSurveyPhotosBySurvey(segmentSurvey);
            if (segmentSurvey == null)
            {
                return HttpNotFound();
            }

            return View(new SegmentSurveyDetailViewModel
            {
                SegmentSurvey = segmentSurvey,
                SurveyPhotos = surveyPhotos
            });
        }

        // GET: SegmentSurveys/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SegmentSurveys/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,BleedLevel,BleedPercentage,BlockLevel,BlockPercentage,CopacesRating,CorrugLevel,CorrugPercentage,CrossSlopeLeft,CrossSlopeRight,EdgeLevel,EdgePercentage,InventId,IsWindshieldSurvey,LaneDirection,LaneNum,LoadLevel1,LoadLevel2,LoadLevel3,LoadLevel4,LossPavLevel,LossPavPercentage,PatchPotholeNum,PlaceId,Rater,RavelLevel,RavelPercentage,ReflectLength,ReflectLevel,ReflectNum,Remarks,RoadId,RoadSurveyDate,RoadSurveyId,RutInWp,RutOutWp,SampleLocation,SegSurveyDate,SequenceId,TreatmentMethod,TreatmentYear,WindshieldScore")] SegmentSurvey segmentSurvey)
        {
            if (ModelState.IsValid)
            {
                //db.SegmentSurveys.Add(segmentSurvey);
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(segmentSurvey);
        }

        // GET: SegmentSurveys/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SegmentSurvey segmentSurvey = await ApiHelper.GetSegmentSurveyById(id);
            if (segmentSurvey == null)
            {
                return HttpNotFound();
            }
            return View(segmentSurvey);
        }

        // POST: SegmentSurveys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,BleedLevel,BleedPercentage,BlockLevel,BlockPercentage,CopacesRating,CorrugLevel,CorrugPercentage,CrossSlopeLeft,CrossSlopeRight,EdgeLevel,EdgePercentage,InventId,IsWindshieldSurvey,LaneDirection,LaneNum,LoadLevel1,LoadLevel2,LoadLevel3,LoadLevel4,LossPavLevel,LossPavPercentage,PatchPotholeNum,PlaceId,Rater,RavelLevel,RavelPercentage,ReflectLength,ReflectLevel,ReflectNum,Remarks,RoadId,RoadSurveyDate,RoadSurveyId,RutInWp,RutOutWp,SampleLocation,SegSurveyDate,SequenceId,TreatmentMethod,TreatmentYear,WindshieldScore")] SegmentSurvey segmentSurvey)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(segmentSurvey).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(segmentSurvey);
        }

        // GET: SegmentSurveys/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SegmentSurvey segmentSurvey = await ApiHelper.GetSegmentSurveyById(id);
            if (segmentSurvey == null)
            {
                return HttpNotFound();
            }
            return View(segmentSurvey);
        }

        // POST: SegmentSurveys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            SegmentSurvey segmentSurvey = await ApiHelper.GetSegmentSurveyById(id);
            //db.SegmentSurveys.Remove(segmentSurvey);
            //await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: SegmentSurveys/Search
        public async Task<ActionResult> Search(SegmentSuveySearchViewModel searchViewModel)
        {
            if (User.Identity.IsAuthenticated)
            {
                var roads = await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId);
                roads.Insert(0, new Road { Id = null, Name = string.Empty });
                ViewBag.RoadList = new SelectList(roads, "Id", "Name", String.Empty);

                if (!searchViewModel.IsEmptyQuery)
                    searchViewModel.Result = (await ApiHelper.SearchSegmentSurveys(
                        start: searchViewModel.StartDate, 
                        end: searchViewModel.EndDate, 
                        placeId: CurrentUser.PlaceId,
                        roadId: searchViewModel.RoadId,
                        maxRating: searchViewModel.MaxRating));
            }

            return View(searchViewModel);
        }
    }
}
