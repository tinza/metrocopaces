﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;

namespace Copaces.CityCounty.Website.Controllers
{
    public class RoadsController : Controller
    {
        /// <summary>
        /// Application DB context
        /// </summary>
        ApplicationDbContext ApplicationDbContext { get; set; }

        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        UserManager<ApplicationUser> UserManager { get; set; }

        public RoadsController()
        {
            ApplicationDbContext = new ApplicationDbContext();
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(this.ApplicationDbContext));
        }

        // GET: Roads/Create
        public ActionResult Create()
        {
            return View();
        }
        
        // POST: Roads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(
                Include =
                    "Id,BridgeNum,BridgeWidth,CulvertAndPipeNum,District,From,FunctionalClass,HasUnfinishedSurvey,Jurisdiction,LaneNum,LastSurveyDate,Name,PavMarkingCondition,PavWidthMax,PavWidthMin,PavWidthTypical,PlaceId,PlaceType,Remarks,ShoulderWidthMax,ShoulderWidthMin,ShoulderWidthTypical,SurfaceType,To,UnpavedShoulderWidth"
                )] Road road)
        {
            if (ModelState.IsValid)
            {
                //db.Roads.Add(road);
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(road);
        }

        // GET: Roads/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var road = await ApiHelper.GetRoadById(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // POST: Roads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            //var road = await db.Roads.FindAsync(id);
            //db.Roads.Remove(road);
            //await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: Roads/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var road = await ApiHelper.GetRoadById(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // GET: Roads/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var road = await ApiHelper.GetRoadById(id);
            if (road == null)
            {
                return HttpNotFound();
            }
            return View(road);
        }

        // POST: Roads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(
                Include =
                    "Id,BridgeNum,BridgeWidth,CulvertAndPipeNum,District,From,FunctionalClass,HasUnfinishedSurvey,Jurisdiction,LaneNum,LastSurveyDate,Name,PavMarkingCondition,PavWidthMax,PavWidthMin,PavWidthTypical,PlaceId,PlaceType,Remarks,ShoulderWidthMax,ShoulderWidthMin,ShoulderWidthTypical,SurfaceType,To,UnpavedShoulderWidth"
                )] Road road)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(road).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(road);
        }

        // GET: Roads
        public async Task<ActionResult> Index(int? pageNum)
        {
            if (User.Identity.IsAuthenticated == false)
                return View();

            var user = UserManager.FindById(User.Identity.GetUserId());
            var roads = await ApiHelper.GetRoadsByPlaceId(user.PlaceId);
            const int countPerPage = 50;
            var pageNumOrDefault = pageNum ?? 1;
            return View(roads.ToPagedList(pageNumOrDefault, countPerPage));
        }
    }
}