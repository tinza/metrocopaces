﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;
using Copaces.CityCounty.Website.Models;
using Copaces.CityCounty.Website.Models.DataObjects;
using Copaces.CityCounty.Website.Utility;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;

namespace Copaces.CityCounty.Website.Controllers
{
    public class SegmentsController : Controller
    {
        /// <summary>
        /// Application DB context
        /// </summary>
        ApplicationDbContext ApplicationDbContext { get; set; }

        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        UserManager<ApplicationUser> UserManager { get; set; }

        private ApplicationUser CurrentUser {
            get {
                return User.Identity.IsAuthenticated ? UserManager.FindById(User.Identity.GetUserId()) : null;
            }
        }

        public SegmentsController()
        {
            ApplicationDbContext = new ApplicationDbContext();
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(this.ApplicationDbContext));
        }

        // GET: Segments
        public async Task<ActionResult> Index(int? pageNum)
        {
            if (User.Identity.IsAuthenticated == false)
                return View();

            var segments = await ApiHelper.GetSegmentsByPlace(new Place() {Id = CurrentUser.PlaceId});

            var pageNumOrDefault = pageNum ?? 1;
            const int countPerPage = 50;

            return View(segments.ToPagedList(pageNumOrDefault, countPerPage));
        }

        // GET: Segments/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Segment segment = await ApiHelper.GetSegmentById(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            return View(segment);
        }

        // GET: Segments/Create
        public async Task<ViewResult> Create()
        {
            ViewBag.RoadId = new SelectList(await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId), "Id", "District");
            return View();
        }

        // POST: Segments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(
                Include =
                    "Id,From,HasCurbOrGutter,InventId,LaneDirection,LaneNum,Length,PlaceId,Remarks,RoadId,SampleLocation,SequenceId,To"
                )] Segment segment)
        {
            if (ModelState.IsValid)
            {
                //db.Segments.Add(segment);
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.RoadId = new SelectList(await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId), "Id", "District");
            return View(segment);
        }

        // GET: Segments/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Segment segment = await ApiHelper.GetSegmentById(id);
            if (segment == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoadId = new SelectList(await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId), "Id", "District");
            return View(segment);
        }

        // POST: Segments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(
                Include =
                    "Id,From,HasCurbOrGutter,InventId,LaneDirection,LaneNum,Length,PlaceId,Remarks,RoadId,SampleLocation,SequenceId,To"
                )] Segment segment)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(segment).State = EntityState.Modified;
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.RoadId = new SelectList(await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId), "Id", "District");
            return View(segment);
        }

        // GET: Segment/History/5
        public async Task<ActionResult> History(int? id)
        {
            if (!id.HasValue)
                return View();

            var surveyList = await ApiHelper.GetSegmentSurveysBySegment(new Segment {InventId = id.Value});

            if (surveyList == null || surveyList.Count == 0)
            {
                ViewBag.HasHistory = false;
                return View();
            }
            else
            {
                ViewBag.HasHistory = true;
                var labelList = surveyList.Select(e => e.SegSurveyDate).ToList();
                var dataList = surveyList.Select(e => e.CopacesRating).ToList();
                var chartData = new
                {
                    labels = labelList,
                    datasets = new[]
                    {
                        new
                        {
                            label = "Rating History",
                            fillColor = "rgba(220,220,220,0.2)",
                            strokeColor = "rgba(220,220,220,1)",
                            pointColor = "rgba(220,220,220,1)",
                            pointStrokeColor = "#fff",
                            pointHighlightFill = "#fff",
                            pointHighlightStroke = "rgba(220,220,220,1)",
                            data = dataList
                        }
                    }
                }; 
                return View(chartData);
            }
        }

        public async Task<ActionResult> Search(SegmentSearchViewModel searchViewModel)
        {
            if (User.Identity.IsAuthenticated == false)
                return View();

            if (!searchViewModel.IsEmptyQuery)
                searchViewModel.Result = await ApiHelper.SearchSegments(
                    from: searchViewModel.From,
                    to: searchViewModel.To,
                    roadId: searchViewModel.RoadId,
                    placeId: CurrentUser.PlaceId);

            var roads = await ApiHelper.GetRoadsByPlaceId(CurrentUser.PlaceId);
            roads.Insert(0, new Road { Id = null, Name = string.Empty });
            ViewBag.RoadList = new SelectList(roads, "Id", "Name", String.Empty);

            return View(searchViewModel);
        }
    }
}
    