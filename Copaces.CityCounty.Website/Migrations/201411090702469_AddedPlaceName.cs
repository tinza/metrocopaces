namespace Copaces.CityCounty.Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlaceName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "PlaceName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "PlaceName");
        }
    }
}
