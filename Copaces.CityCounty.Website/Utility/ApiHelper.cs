﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Copaces.CityCounty.Website.Models.DataObjects;
using Newtonsoft.Json;

namespace Copaces.CityCounty.Website.Utility
{
    public static class ApiHelper
    {
        private static string BaseURL = "https://copacescc.azure-mobile.net/";
        public static HttpClient Client { get; set; }

        static ApiHelper()
        {
            Client = new HttpClient();
            Client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            Client.DefaultRequestHeaders.Add("x-zumo-application", "YidgbNavkcizcZIcYtVWCENpqKwVLs20");
        }

        static async Task<string> GetAsyncWithCred(this HttpClient client, string apiUrl)
        {
            string responseBody;

            using (var response = client.GetAsync(BaseURL + apiUrl).Result)
            {
                response.EnsureSuccessStatusCode();
                responseBody = await response.Content.ReadAsStringAsync();
            }
            return responseBody;
        }

        public static async Task<List<Place>> GetPlaces()
        {
            var response = await Client.GetAsyncWithCred("tables/place");
            var list = JsonConvert.DeserializeObject<List<Place>>(response);
            return list;
        }

        public static async Task<Place> GetPlaceById(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;
            var url = String.Format("tables/place/{0}", id);
            return JsonConvert.DeserializeObject<Place>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Road>> GetRoadsByPlace(Place place)
        {
            if (place.Id == null) return null;
            var url = String.Format("tables/road?$filter=(placeId+eq+'{0}')", place.Id);
            return JsonConvert.DeserializeObject<List<Road>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<Road> GetRoadById(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;
            var url = String.Format("tables/road/{0}", id);
            return JsonConvert.DeserializeObject<Road>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Road>> GetRoadsByPlaceId(string id)
        {
            return await GetRoadsByPlace(new Place {Id = id});
        }

        public static async Task<List<RoadSurvey>> GetRoadSurveysByRoad(Road road)
        {
            if (road.Id == null) return null;
            var url = String.Format("tables/roadsurvey?$filter=roadId eq {0}", road.Id);
            return JsonConvert.DeserializeObject<List<RoadSurvey>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Segment>> GetSegmentsByPlace(Place place)
        {
            if (place.Id == null) return null;
            var url = String.Format("tables/segment?$filter=placeId eq '{0}'", place.Id);
            return JsonConvert.DeserializeObject<List<Segment>>(await Client.GetAsyncWithCred(url))
                .OrderBy(s => s.InventId).ToList();
        }

        public static async Task<List<Segment>> SearchSegments(
            string from = null, 
            string to = null, 
            string roadId = null,
            string placeId = null)
        {
            var filters = new List<String>();
            if (from != null)
                filters.Add(String.Format("(substringof('{0}', from))", from));
            if (to != null)
                filters.Add(String.Format("(substringof('{0}', to))", to));
            if (placeId != null)
                filters.Add(String.Format("(placeId eq '{0}')", placeId));
            if (roadId != null)
                filters.Add(String.Format("(roadId eq '{0}')", roadId));

            var finalFilter = String.Join(" and ", filters);
            var url = String.IsNullOrEmpty(finalFilter) ? "tables/segment" : String.Format("tables/segment?$filter={0}", finalFilter);
            return JsonConvert.DeserializeObject<List<Segment>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<Segment>> GetSegmentsByRoad(Road road)
        {
            if (road.Id == null) return null;
            var url = String.Format("tables/segment?$filter=roadId eq '{0}'", road.Id);
            return JsonConvert.DeserializeObject<List<Segment>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysByRoadSurvey(RoadSurvey roadSurvey)
        {
            if (roadSurvey.Id == null) return null;
            var url = String.Format("tables/segmentSurvey?$filter=roadSurveyId eq '{0}'", roadSurvey.Id);
            return JsonConvert.DeserializeObject<List<SegmentSurvey>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysBySegment(Segment segment)
        {
            var url = String.Format("tables/segmentsurvey?$filter=inventId eq {0}", segment.InventId);
            var response = await Client.GetAsyncWithCred(url);
            return (JsonConvert.DeserializeObject<List<SegmentSurvey>>(response)).OrderBy(ss => ss.SegSurveyDate).ToList();
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysByPlace(Place place)
        {
            if (place.Id == null) return null;
            var url = String.Format("tables/segmentSurvey?$filter=placeId eq '{0}'", place.Id);
            return (JsonConvert.DeserializeObject<List<SegmentSurvey>>(await Client.GetAsyncWithCred(url)))
                .OrderByDescending(ss => ss.SegSurveyDate).ToList();
        }

        public static async Task<SegmentSurvey> GetSegmentSurveyById(String id)
        {
            var url = String.Format("tables/segmentSurvey/{0}", id);
            return JsonConvert.DeserializeObject<SegmentSurvey>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<Segment> GetSegmentById(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;
            var url = String.Format("tables/segment/{0}", id);
            return JsonConvert.DeserializeObject<Segment>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SegmentSurvey>> SearchSegmentSurveys(DateTime? start = null, DateTime? end = null, string placeId = null, string roadId = null, int? maxRating = null)
        {
            var filters = new List<String>();
            if (start.HasValue)
                filters.Add(String.Format("(segSurveyDate ge datetime'{0}')", start.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
            if (end.HasValue)
                filters.Add(string.Format("(segSurveyDate le datetime'{0}')", end.Value.ToString("yyyy-MM-ddTHH:mm:ss")));
            if (placeId != null)
                filters.Add(String.Format("(placeId eq '{0}')", placeId));
            if (roadId != null)
                filters.Add(String.Format("(roadId eq '{0}')", roadId));
            if (maxRating != null)
                filters.Add(String.Format("(copacesRating le {0})", maxRating.Value));

            var finalFilter = String.Join(" and ", filters);
            var url = String.IsNullOrEmpty(finalFilter) ? "tables/segmentsurvey" : String.Format("tables/segmentsurvey?$filter={0}", finalFilter);
            return JsonConvert.DeserializeObject<List<SegmentSurvey>>(await Client.GetAsyncWithCred(url));
        }

        public static async Task<List<SurveyPhoto>>  GetSurveyPhotosBySurvey(SegmentSurvey segmentSurvey)
        {
            var url = String.Format("tables/surveyPhoto?$filter=segmentSurveyId eq '{0}'", segmentSurvey.Id);
            return JsonConvert.DeserializeObject<List<SurveyPhoto>>(await Client.GetAsyncWithCred(url));
        }
    }
}