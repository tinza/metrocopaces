﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Copaces.CityCounty.Website.Startup))]
namespace Copaces.CityCounty.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
