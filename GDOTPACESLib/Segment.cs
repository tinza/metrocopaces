﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace GDOTPACESLib
{
    public class Segment
    {
        #region Private variables and their property methods
        ConfigFileReader cfr = new ConfigFileReader();
        int _outsideWP = 0;
        int _insideWP = 0;
        int _loadCrackingSeverityLevel1Percentage = 0;
        int _loadCrackingSeverityLevel2Percentage = 0;
        int _loadCrackingSeverityLevel3Percentage = 0;
        int _loadCrackingSeverityLevel4Percentage = 0;
        int _blockCrackingPercentage = 0;
        int _blockCrackingSeverity = 0;
        int _ravelingPercentage = 0;
        int _ravelingSeverity = 0;
        int _edgeDistressPercentage = 0;
        int _edgeDistressSeverity = 0;
        int _bleedingPercentage = 0;
        int _bleedingSeverity = 0;
        int _lossOfPavementPercentage = 0;
        int _lossOfPavementSeverity = 0;
        int _corrugationPercentage = 0;
        int _corrugationSeverity = 0;
        int _reflectionCrackNumber = 0;
        int _reflectionCrackTotalLength = 0;
        int _reflectionCrackSeverity = 0;
        int _patchesAndPotholes = 0;
        int _crossSlopeLeft = 0;
        int _crossSlopeRight = 0;

        int _rutDepthDeduct = 0;
        int _loadCrackingDeduct = 0;
        int _blockCrackingDeduct = 0;
        int _ravelingDeduct = 0;
        int _edgeDistressDeduct = 0;
        int _bleedingDeduct = 0;
        int _lossOfPavementDeduct = 0;
        int _corrugationDeduct = 0;
        int _reflectionDeduct = 0;
        int _patchesAndPotholesDeduct = 0;
        int _crossSlopeDeduct = 0;
        int _loadCrackingSeverity1Deduct = 0;
        int _loadCrackingSeverity2Deduct = 0;
        int _loadCrackingSeverity3Deduct = 0;
        int _loadCrackingSeverity4Deduct = 0;

        int _segmentRating = 0;

        int _rutDepthMax = 0;
        int _crossSlopeMax = 0;

        /// <summary>
        /// Holds the maximum of Cross slope values.
        /// </summary>
        public int CrossSlopeMax
        {
            get { return _crossSlopeMax; }
            private set { _crossSlopeMax = value; }
        }

        /// <summary>
        /// Holds the maximum of the rut depths.
        /// </summary>
        public int RutDepthMax
        {
            get { return _rutDepthMax; }
            private set { _rutDepthMax = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 1
        /// </summary>
        public int LoadCrackingSeverity1Deduct
        {
            get { return _loadCrackingSeverity1Deduct; }
            private set { _loadCrackingSeverity1Deduct = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 2
        /// </summary>
        public int LoadCrackingSeverity2Deduct
        {
            get { return _loadCrackingSeverity2Deduct; }
            private set { _loadCrackingSeverity2Deduct = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 3
        /// </summary>
        public int LoadCrackingSeverity3Deduct
        {
            get { return _loadCrackingSeverity3Deduct; }
            private set { _loadCrackingSeverity3Deduct = value; }
        }

        /// <summary>
        /// Holds the load cracking deduct for severity level 4
        /// </summary>
        public int LoadCrackingSeverity4Deduct
        {
            get { return _loadCrackingSeverity4Deduct; }
            private set { _loadCrackingSeverity4Deduct = value; }
        }

        /// <summary>
        /// Holds the deduct value for Rut depth.
        /// </summary>
        public int RutDepthDeduct
        {
            get { return _rutDepthDeduct; }
            private set { _rutDepthDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value for Load Cracking.
        /// </summary>
        public int LoadCrackingDeduct
        {
            get { return _loadCrackingDeduct; }
            private set { _loadCrackingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value for Block Cracking.
        /// </summary>
        public int BlockCrackingDeduct
        {
            get { return _blockCrackingDeduct; }
            private set { _blockCrackingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Raveling.
        /// </summary>
        public int RavelingDeduct
        {
            get { return _ravelingDeduct; }
            private set { _ravelingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Edge Distress.
        /// </summary>
        public int EdgeDistressDeduct
        {
            get { return _edgeDistressDeduct; }
            private set { _edgeDistressDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Bleeding.
        /// </summary>
        public int BleedingDeduct
        {
            get { return _bleedingDeduct; }
            private set { _bleedingDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Loss of Pavement section.
        /// </summary>
        public int LossOfPavementDeduct
        {
            get { return _lossOfPavementDeduct; }
            private set { _lossOfPavementDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Corrugation.
        /// </summary>
        public int CorrugationDeduct
        {
            get { return _corrugationDeduct; }
            private set { _corrugationDeduct = value; }
        }
        /// <summary>
        /// Holds the deduct value returned for Reflection Cracking.
        /// </summary>
        public int ReflectionDeduct
        {
            get { return _reflectionDeduct; }
            private set { _reflectionDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Patches and potholes.
        /// </summary>
        public int PatchesAndPotholesDeduct
        {
            get { return _patchesAndPotholesDeduct; }
            private set { _patchesAndPotholesDeduct = value; }
        }

        /// <summary>
        /// Holds the deduct value returned for Cross Slope.
        /// </summary>
        public int CrossSlopeDeduct
        {
            get { return _crossSlopeDeduct; }
            private set { _crossSlopeDeduct = value; }
        }

        /// <summary>
        /// Holds the right side cross slope values.Is not used in calculation of Rating.
        /// </summary>
        public int CrossSlopeRight
        {
            get
            {
                return _crossSlopeRight;
            }
            set
            {
                _crossSlopeRight = value;
            }
        }

        /// <summary>
        /// Holds the left side cross slope values.Is not used in calculation of Rating.
        /// </summary>
        public int CrossSlopeLeft
        {
            get
            {
                return _crossSlopeLeft;
            }
            set
            {
                _crossSlopeLeft = value;
            }
        }

        /// <summary>
        /// Holds the number of Cracks caused by reflection. Is not used in calculation of Rating.
        /// </summary>
        public int ReflectionCrackNumber
        {
            get
            {
                return _reflectionCrackNumber;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _reflectionCrackNumber = value;
            }
        }

        /// <summary>
        /// Holds the Outside Wheel Path value. Expects an integer between 0 and 8 inclusive.
        /// </summary>
        public int OutsideWP
        {
            get
            {
                return _outsideWP;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _outsideWP = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Inside Wheel Path value. Expects an integer between 0 and 8 inclusive.
        /// </summary>
        public int InsideWP
        {
            get { return _insideWP; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _insideWP = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Load Cracking for Severity 1. Expects an integer between 0 and 100 inclusive. 
        /// </summary>
        public int LoadCrackingSeverityLevel1Percentage
        {
            get { return _loadCrackingSeverityLevel1Percentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (LoadCrackingSeverityLevel1Percentage + LoadCrackingSeverityLevel2Percentage + LoadCrackingSeverityLevel3Percentage + LoadCrackingSeverityLevel4Percentage > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _loadCrackingSeverityLevel1Percentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Load Cracking for Severity 2. Expects an integer between 0 and 100 inclusive. 
        /// </summary>
        public int LoadCrackingSeverityLevel2Percentage
        {
            get { return _loadCrackingSeverityLevel2Percentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (LoadCrackingSeverityLevel1Percentage + LoadCrackingSeverityLevel2Percentage + LoadCrackingSeverityLevel3Percentage + LoadCrackingSeverityLevel4Percentage > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _loadCrackingSeverityLevel2Percentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Load Cracking for Severity 3. Expects an integer between 0 and 100 inclusive. 
        /// </summary>
        public int LoadCrackingSeverityLevel3Percentage
        {
            get { return _loadCrackingSeverityLevel3Percentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (LoadCrackingSeverityLevel1Percentage + LoadCrackingSeverityLevel2Percentage + LoadCrackingSeverityLevel3Percentage + LoadCrackingSeverityLevel4Percentage > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _loadCrackingSeverityLevel3Percentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Load Cracking for Severity 4. Expects an integer between 0 and 100 inclusive. 
        /// </summary>
        public int LoadCrackingSeverityLevel4Percentage
        {
            get { return _loadCrackingSeverityLevel4Percentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (LoadCrackingSeverityLevel1Percentage + LoadCrackingSeverityLevel2Percentage + LoadCrackingSeverityLevel3Percentage + LoadCrackingSeverityLevel4Percentage > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _loadCrackingSeverityLevel4Percentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Block Cracking. Expects an integer between 0 and 100 inclusive. 
        /// Ignored if BlockCrackingSeverity is not supplied.
        /// </summary>
        public int BlockCrackingPercentage
        {
            get { return _blockCrackingPercentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _blockCrackingPercentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Severity of Block Cracking. Expects an integer between 0 and 3 inclusive. 
        /// Ignored if BlockCrackingPercentage is not supplied.
        /// </summary>
        public int BlockCrackingSeverity
        {
            get { return _blockCrackingSeverity; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _blockCrackingSeverity = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Raveling. Expects an integer between 0 and 100 inclusive. 
        /// Ignored if RavelingSeverity is not supplied.
        /// </summary>
        public int RavelingPercentage
        {
            get { return _ravelingPercentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _ravelingPercentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Severity of Raveling. Expects an integer between 0 and 3 inclusive. 
        /// Ignored if RavelingPercentage is not supplied.
        /// </summary>
        public int RavelingSeverity
        {
            get { return _ravelingSeverity; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _ravelingSeverity = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Edge Distress. Expects an integer between 0 and 100 inclusive. 
        /// Ignored if EdgeDistressSeverity is not supplied.
        /// </summary>
        public int EdgeDistressPercentage
        {
            get { return _edgeDistressPercentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _edgeDistressPercentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Severity of Edge Distress. Expects an integer between 0 and 3 inclusive. 
        /// Ignored if EdgeDistressPercentage is not supplied.
        /// </summary>
        public int EdgeDistressSeverity
        {
            get { return _edgeDistressSeverity; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _edgeDistressSeverity = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Bleeding. Expects an integer between 0 and 100 inclusive. 
        /// Ignored ifBleedingSeverity is not supplied.
        /// </summary>
        public int BleedingPercentage
        {
            get { return _bleedingPercentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _bleedingPercentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Severity of Bleeding. Expects an integer between 0 and 2 inclusive. 
        /// Ignored if BleedingPercentage is not supplied.
        /// </summary>
        public int BleedingSeverity
        {
            get { return _bleedingSeverity; }
            set
            {
                if (value < 0 || value > 2)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _bleedingSeverity = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Loss of Pavement Section. Expects an integer between 0 and 100 inclusive. 
        /// Ignored if LossOfPavementSeverity is not supplied.
        /// </summary>
        public int LossOfPavementPercentage
        {
            get { return _lossOfPavementPercentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _lossOfPavementPercentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Severity of Loss of Pavement Section. Expects an integer between 0 and 3 inclusive. 
        /// Ignored if LossOfPavementPercentage is not supplied.
        /// </summary>
        public int LossOfPavementSeverity
        {
            get { return _lossOfPavementSeverity; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _lossOfPavementSeverity = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Percentage of Corrugation. Expects an integer between 0 and 100 inclusive. 
        /// Ignored if CorrugationSeverity is not supplied.
        /// </summary>
        public int CorrugationPercentage
        {
            get { return _corrugationPercentage; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _corrugationPercentage = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Severity of Corrugation. Expects an integer between 0 and 3 inclusive. 
        /// Ignored if CorrugationPercentage is not supplied.
        /// </summary>
        public int CorrugationSeverity
        {
            get { return _corrugationSeverity; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _corrugationSeverity = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the length of Reflection Crack. Expects an integer between 0 and 100 inclusive. 
        /// Ignored if ReflectionCrackSeverity is not supplied.
        /// </summary>
        public int ReflectionCrackTotalLength
        {
            get { return _reflectionCrackTotalLength; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _reflectionCrackTotalLength = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the Severity of Reflection Cracking. Expects an integer between 0 and 3 inclusive. 
        /// Ignored if ReflectionCrackTotalLength is not supplied.
        /// </summary>
        public int ReflectionCrackSeverity
        {
            get { return _reflectionCrackSeverity; }
            set
            {
                if (value < 0 || value > 3)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _reflectionCrackSeverity = value;
                CalculateSegmentRating();
            }
        }

        /// <summary>
        /// Holds the value for number of Patches and Potholes. Expects an integer greater than or equal to 0.
        /// </summary>
        public int PatchesAndPotholes
        {
            get { return _patchesAndPotholes; }
            set
            {
                _patchesAndPotholes = value;
                CalculateSegmentRating();
            }
        }


        public int SegmentRating
        {
            get { return _segmentRating; }
            private set { _segmentRating = value; }
        }

        #endregion

        /// <summary>
        /// Takes an array of distress values at the following positions.
        /// 0  - Outside WP.
        /// 1  - Inside WP.
        /// 2  - Load Cracking Severity 1.
        /// 3  - Load Cracking Severity 2.
        /// 4  - Load Cracking Severity 3.
        /// 5  - Load Cracking Severity 4.
        /// 6  - Block Cracking Percentage. 
        /// 7  - Block Cracking Severity.
        /// 8  - Reflection Crack Number.
        /// 9  - Reflection Length.
        /// 10 - Reflection Severity.
        /// 11 - Raveling Percentage.      
        /// 12 - Raveling Severity.
        /// 13 - Edge Distress Percentage. 
        /// 14 - Edge Distress Severity.
        /// 15 - Bleeding Percentage.  
        /// 16 - Bleeding Severity.
        /// 17 - Corrugation Percentage.
        /// 18 - Corrugation Severity.
        /// 19 - Loss of Pavement Section Percentage.
        /// 20 - Loss of Pavement Section Severity.
        /// 21 - Cross Slope Left.
        /// 22 - Cross Slope Right.
        /// 23 - Patches and Potholes count.
        /// Throws an ArgumentOutOfRange exception if any of the values are out of expected range.
        /// </summary>
        public Segment(int[] distressValues)
        {

            _outsideWP = distressValues[0];
            _insideWP = distressValues[1];
            _loadCrackingSeverityLevel1Percentage = distressValues[2];
            _loadCrackingSeverityLevel2Percentage = distressValues[3];
            _loadCrackingSeverityLevel3Percentage = distressValues[4];
            _loadCrackingSeverityLevel4Percentage = distressValues[5];
            _blockCrackingPercentage = distressValues[6];
            _blockCrackingSeverity = distressValues[7];
            _reflectionCrackNumber = distressValues[8];
            _reflectionCrackTotalLength = distressValues[9];
            _reflectionCrackSeverity = distressValues[10];
            _ravelingPercentage = distressValues[11];
            _ravelingSeverity = distressValues[12];
            _edgeDistressPercentage = distressValues[13];
            _edgeDistressSeverity = distressValues[14];
            _bleedingPercentage = distressValues[15];
            _bleedingSeverity = distressValues[16];
            _corrugationPercentage = distressValues[17];
            _corrugationSeverity = distressValues[18];
            _lossOfPavementPercentage = distressValues[19];
            _lossOfPavementSeverity = distressValues[20];
            _crossSlopeLeft = distressValues[21];
            _crossSlopeRight = distressValues[22];
            _patchesAndPotholes = distressValues[23];

            CalculateSegmentRating();
        }

        /// <summary>
        /// Constructor that does not take any parameters. 
        /// Calling method should initialize distress values by hand after creating object.
        /// </summary>
        public Segment()
        {
            CalculateSegmentRating();
        }

        /// <summary>
        /// Call whenever segment rating needs to be calculated.
        /// </summary>
        private void CalculateSegmentRating()
        {
            SegmentRating = 0;
            if (OutsideWP >= InsideWP)
            {
                RutDepthDeduct = cfr.GetRutDepthDistress(OutsideWP);
                RutDepthMax = OutsideWP;
            }
            else
            {
                RutDepthDeduct = cfr.GetRutDepthDistress(InsideWP);
                RutDepthMax = InsideWP;
            }
            if (CrossSlopeLeft >= CrossSlopeRight)
            {
                CrossSlopeMax = CrossSlopeLeft;
            }
            else
            {
                CrossSlopeMax = CrossSlopeRight;
            }

            LoadCrackingSeverity1Deduct = cfr.GetLoadCrackingDistress(1, LoadCrackingSeverityLevel1Percentage);
            LoadCrackingSeverity2Deduct = cfr.GetLoadCrackingDistress(2, LoadCrackingSeverityLevel2Percentage);
            LoadCrackingSeverity3Deduct = cfr.GetLoadCrackingDistress(3, LoadCrackingSeverityLevel3Percentage);
            LoadCrackingSeverity4Deduct = cfr.GetLoadCrackingDistress(4, LoadCrackingSeverityLevel4Percentage);

            LoadCrackingDeduct = LoadCrackingSeverity1Deduct;

            if (LoadCrackingSeverity2Deduct > LoadCrackingDeduct)
            {
                LoadCrackingDeduct = LoadCrackingSeverity2Deduct;
            }
            if (LoadCrackingSeverity3Deduct > LoadCrackingDeduct)
            {
                LoadCrackingDeduct = LoadCrackingSeverity3Deduct;
            }
            if (LoadCrackingSeverity4Deduct > LoadCrackingDeduct)
            {
                LoadCrackingDeduct = LoadCrackingSeverity4Deduct;
            }


            BleedingDeduct = cfr.GetBleedingDistress(BleedingSeverity, BleedingPercentage);
            BlockCrackingDeduct = cfr.GetBlockCrackingDistress(BlockCrackingSeverity, BlockCrackingPercentage);
            CorrugationDeduct = cfr.GetCorrugationDistress(CorrugationSeverity, CorrugationPercentage);
            EdgeDistressDeduct = cfr.GetEdgeDistress(EdgeDistressSeverity, EdgeDistressPercentage);
            LossOfPavementDeduct = cfr.GetLossSectionDistress(LossOfPavementSeverity, LossOfPavementPercentage);
            PatchesAndPotholesDeduct = cfr.GetPatchAndPotholesDistress(PatchesAndPotholes);
            RavelingDeduct = cfr.GetRavelingDistress(RavelingSeverity, RavelingPercentage);
            ReflectionDeduct = cfr.GetReflectionCrackingDistress(ReflectionCrackSeverity, ReflectionCrackTotalLength);

            SegmentRating = 100 - (RutDepthDeduct + LoadCrackingDeduct + BleedingDeduct + BlockCrackingDeduct + CorrugationDeduct + EdgeDistressDeduct + LossOfPavementDeduct + PatchesAndPotholesDeduct + RavelingDeduct + ReflectionDeduct);

            if (SegmentRating < 0)
            {
                SegmentRating = 0;
            }
        }
    }
}