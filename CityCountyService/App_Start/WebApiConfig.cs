﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Xml.Linq;
using CityCountyService.Utility;
using Microsoft.WindowsAzure.Mobile.Service;
using CityCountyService.Models;
using CityCountyService.DataObjects;
using System.Web;
using System.Net;

namespace CityCountyService
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            // Use this class to set configuration options for your mobile service
            ConfigOptions options = new ConfigOptions();

            // Use this class to set WebAPI configuration options
            HttpConfiguration config = ServiceConfig.Initialize(new ConfigBuilder(options));

            // To display errors in the browser during development, uncomment the following
            // line. Comment it out again when you deploy your service for production use.
            // config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            Database.SetInitializer(new CityCountyInitializer());
        }
    }

    public class CityCountyInitializer : ClearDatabaseSchemaIfModelChanges<CopacesCcDbContext>
    {
        protected override void Seed(CopacesCcDbContext context)
        {
            var baseUrl = @"https://bitbucket.org/tinza/metrocopaces/raw/ac64f21475a3ccc2b670a5f54581a3af89c4cf32/CityCountyService/App_Data/Seeding/";
            var place = baseUrl + @"Place.xml";
            DataSeeder.SeedPlacesFromXml(context, place);

            var roads = new[] { "Road_CHATHAM", "Road_NEWTON" }
                        .Select(road => String.Format(@"{0}Roads/{1}.xml", baseUrl, road)).ToList();
            foreach (var road in roads)
                DataSeeder.SeedRoadsFromXml(context, road);

            var segments = new[] { "Segment_CHATHAM", "Segment_NEWTON" }
                        .Select(segment => String.Format(@"{0}Segments/{1}.xml", baseUrl, segment)).ToList();
            foreach (var segment in segments)
                DataSeeder.SeedSegmentsFromXml(context, segment);
            
            var segSurveys = new[] { "SegSurvey" }
                        .Select(ss => String.Format(@"{0}SegmentSurveys/{1}.xml", baseUrl, ss)).ToList();
            foreach (var segSurvey in segSurveys)
                DataSeeder.SeedSegSurveysFromXml(context, segSurvey);

            //var roadSurveys = new string[] { "RoadSurvey" }
            //.Select(rs => String.Format(@"{0}RoadSurveys/{1}.xml", baseUrl, rs)).ToList();
            //foreach (var roadSurvey in roadSurveys)
            //    DataSeeder.SeedRoadSurveysFromXml(context, roadSurvey);

            base.Seed(context);
        }
    }
}

