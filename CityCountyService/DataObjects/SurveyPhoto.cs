﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace CityCountyService.DataObjects
{
    public class SurveyPhoto : EntityData
    {
        public string SegmentSurveyId { get; set; }

        public string resourceName { get; set; }
        public string imageUri { get; set; }

        // The image location
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTime? TimeTaken { get; set; }

        public SegmentSurvey SegmentSurvey { get; set; }
    }
}