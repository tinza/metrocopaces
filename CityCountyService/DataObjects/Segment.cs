﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.WindowsAzure.Mobile.Service;

namespace CityCountyService.DataObjects
{
    public class Segment : EntityData
    {
        public String From { get; set; }

        public String HasCurbOrGutter { get; set; }

        [Key]
        public int InventId { get; set; }

        public String LaneDirection { get; set; }

        public int? LaneNum { get; set; }

        public double? Length { get; set; }

        public Place Place { get; set; }

        [ForeignKey("Place")]
        public String PlaceId { get; set; }

        public String Remarks { get; set; }

        public Road Road { get; set; }

        [ForeignKey("Road")]
        public String RoadId { get; set; }

        public String SampleLocation { get; set; }

        public int? SequenceId { get; set; }

        public String To { get; set; }

        public List<SegmentSurvey> SegmentSurveys { get; set; }
    }
}