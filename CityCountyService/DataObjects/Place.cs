﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace CityCountyService.DataObjects
{
    public class Place : EntityData
    {
        public String Name { get; set; }
        public String Type { get; set; }
        public int? CountyNo { get; set; }
        public int? District { get; set; }

        public List<Road> Roads { get; set; }
    }
}