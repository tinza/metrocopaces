﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Mobile.Service.Tables;
using CityCountyService.DataObjects;

namespace CityCountyService.Models
{
    public class CopacesCcDbContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
        //
        // To enable Entity Framework migrations in the cloud, please ensure that the 
        // service name, set by the 'MS_MobileServiceName' AppSettings in the local 
        // Web.config, is the same as the service name when hosted in Azure.
        private const string ConnectionStringName = "Name=MS_TableConnectionString";

        public CopacesCcDbContext()
            : base(ConnectionStringName)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            string schema = ServiceSettingsDictionary.GetSchemaName();
            if (!string.IsNullOrEmpty(schema))
            {
                modelBuilder.HasDefaultSchema(schema);
            }
            // modelBuilder.HasDefaultSchema("CopaceCityCounty");

            // Auto-increment the primary key
            modelBuilder.Entity<Segment>()
                .HasKey(t => new { t.InventId })
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<SurveyPhoto>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));
        }

        public DbSet<Place> Places { get; set; }
        public DbSet<Road> Roads { get; set; }
        public DbSet<Segment> Segments { get; set; }
        public DbSet<RoadSurvey> RoadSurveys { get; set; }
        public DbSet<SegmentSurvey> SegmentSurveys { get; set; }
        public DbSet<SurveyPhoto> SurveyPhotoes { get; set; }
    }
}
