﻿using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using CityCountyService.Utility;
using Microsoft.WindowsAzure.Mobile.Service;
using CityCountyService.Models;
using CityCountyService.DataObjects;

namespace CityCountyService.Controllers
{
    public class SegmentController : TableController<Segment>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            CopacesCcDbContext context = new CopacesCcDbContext();
            DomainManager = new EntityDomainManager<Segment>(context, Request, Services);
        }

        [Queryable(MaxTop = 1000)]
        // GET tables/Segment
        public IQueryable<Segment> GetAllSegment()
        {
            return Query(); 
        }

        // GET tables/Segment/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Segment> GetSegment(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Segment/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Segment> PatchSegment(string id, Delta<Segment> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Segment/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public async Task<IHttpActionResult> PostSegment(Segment item)
        {
            Segment current = await InsertAsync(item);
            RelationHelper.ProcessNewSegment(current);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Segment/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteSegment(string id)
        {
             return DeleteAsync(id);
        }

    }
}