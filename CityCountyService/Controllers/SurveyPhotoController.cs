﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using CityCountyService.Models;
using CityCountyService.DataObjects;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CityCountyService.Controllers
{
    public class SurveyPhotoController : TableController<SurveyPhoto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            var context = new CopacesCcDbContext();
            DomainManager = new EntityDomainManager<SurveyPhoto>(context, Request, Services);
        }

        [Queryable(MaxTop = 1000)]
        // GET tables/SurveyPhoto
        public IQueryable<SurveyPhoto> GetAllSurveyPhoto()
        {
            return Query();
        }

        // GET tables/SurveyPhoto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<SurveyPhoto> GetSurveyPhoto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/SurveyPhoto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<SurveyPhoto> PatchSurveyPhoto(string id, Delta<SurveyPhoto> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/SurveyPhoto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public async Task<IHttpActionResult> PostSurveyPhoto(SurveyPhoto item)
        {
            string storageAccountName;
            string storageAccountKey;

            // Try to get the Azure storage account token from app settings.  
            if (!(Services.Settings.TryGetValue("STORAGE_ACCOUNT_NAME", out storageAccountName) |
                  Services.Settings.TryGetValue("STORAGE_ACCOUNT_ACCESS_KEY", out storageAccountKey)))
            {
                Services.Log.Error("Could not retrieve storage account settings.");
            }

            // Set the URI for the Blob Storage service.
            var blobEndpoint = new Uri(string.Format("https://{0}.blob.core.windows.net", storageAccountName));

            // Set the URL used to store the image.
            item.imageUri = string.Format("{0}{1}/{2}", blobEndpoint,
                "segmentsurvey-images", item.resourceName);

            // Complete the insert operation.
            SurveyPhoto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new {id = current.Id}, current);
        }

        // DELETE tables/SurveyPhoto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteSurveyPhoto(string id)
        {
            return DeleteAsync(id);
        }
    }
}