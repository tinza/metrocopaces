﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CityCountyService.Controllers
{
    public class FileUploadController : ApiController
    {
        public ApiServices Services { get; set; }

        // GET api/FileUpload
        public async Task<string> Get()
        {
            string storageAccountName;
            string storageAccountKey;

            // Try to get the Azure storage account token from app settings.  
            if (!(Services.Settings.TryGetValue("STORAGE_ACCOUNT_NAME", out storageAccountName) |
                  Services.Settings.TryGetValue("STORAGE_ACCOUNT_ACCESS_KEY", out storageAccountKey)))
            {
                Services.Log.Error("Could not retrieve storage account settings.");
            }

            // Set the URI for the Blob Storage service.
            var blobEndpoint = new Uri(string.Format("https://{0}.blob.core.windows.net", storageAccountName));

            // Create the BLOB service client.
            var blobClient = new CloudBlobClient(blobEndpoint,
                new StorageCredentials(storageAccountName, storageAccountKey));

            // Create a container, if it doesn't already exist.
            var container = blobClient.GetContainerReference("segmentsurvey-images");
            await container.CreateIfNotExistsAsync();

            // Create a shared access permission policy, and 
            // enable anonymous read access to BLOBs.
            container.SetPermissions(new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });

            // Define a policy that gives write access to the container for 5 minutes.                                   
            var sasPolicy = new SharedAccessBlobPolicy
            {
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(1),
                Permissions = SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Read
            };

            // Get the SAS as a string.
            var sasQueryString = container.GetSharedAccessSignature(sasPolicy);
            return sasQueryString;
        }

    }
}
