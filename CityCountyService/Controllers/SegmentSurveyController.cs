﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using CityCountyService.Models;
using CityCountyService.DataObjects;
using Microsoft.WindowsAzure.Mobile.Service;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CityCountyService.Controllers
{
    public class SegmentSurveyController : TableController<SegmentSurvey>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            var context = new CopacesCcDbContext();
            DomainManager = new EntityDomainManager<SegmentSurvey>(context, Request, Services);
        }

        [Queryable(MaxTop = 1000)]
        // GET tables/SegmentSurvey
        public IQueryable<SegmentSurvey> GetAllSegmentSurvey()
        {
            return Query();
        }

        // GET tables/SegmentSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<SegmentSurvey> GetSegmentSurvey(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/SegmentSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<SegmentSurvey> PatchSegmentSurvey(string id, Delta<SegmentSurvey> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/SegmentSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public async Task<IHttpActionResult> PostSegmentSurvey(SegmentSurvey item)
        {
            SegmentSurvey current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/SegmentSurvey/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteSegmentSurvey(string id)
        {
            return DeleteAsync(id);
        }
    }
}