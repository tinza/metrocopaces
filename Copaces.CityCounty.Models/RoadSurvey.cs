﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.WindowsAzure.Mobile.Service;

namespace Copaces.CityCounty.Models
{
    public class RoadSurvey : EntityData
    {
        public double? BleedAvg { get; set; }

        public double? BleedDeduct { get; set; }

        public double? BleedLevel { get; set; }

        public double? BlockAvg { get; set; }

        public double? BlockDeduct { get; set; }

        public double? BlockLevel { get; set; }

        public double? CorrugAvg { get; set; }

        public double? CorrugDeduct { get; set; }

        public double? CorrugLevel { get; set; }

        public DateTime Date { get; set; }

        public double? EdgeAvg { get; set; }

        public double? EdgeDeduct { get; set; }

        public double? EdgeLevel { get; set; }

        public DateTime? FinishDate { get; set; }

        public bool IsFinished { get; set; }

        public double? LoadLevel1Avg { get; set; }

        public double? LoadLevel1Deduct { get; set; }

        public double? LoadLevel2Avg { get; set; }

        public double? LoadLevel2Deduct { get; set; }

        public double? LoadLevel3Avg { get; set; }

        public double? LoadLevel3Deduct { get; set; }

        public double? LoadLevel4Avg { get; set; }

        public double? LoadLevel4Deduct { get; set; }

        public double? LossAvg { get; set; }

        public double? LossDeduct { get; set; }

        public double? LossLevel { get; set; }

        public double? PatchAvg { get; set; }

        public double? PatchDeduct { get; set; }

        public Place Place { get; set; }

        [ForeignKey("Place")]
        public String PlaceId { get; set; }

        public double? ProjectRating { get; set; }

        public double? RavelAvg { get; set; }

        public double? RavelDeduct { get; set; }

        public double? RavelLevel { get; set; }

        public double? ReflectAvg { get; set; }

        public double? ReflectDeduct { get; set; }

        public double? ReflectLevel { get; set; }

        public Road Road { get; set; }

        [ForeignKey("Road")]
        public String RoadId { get; set; }

        public double? RutAvg { get; set; }

        public double? RutDeduct { get; set; }

        public double? SlopeAvg { get; set; }

        public double? SlopeDeduct { get; set; }
    }
}