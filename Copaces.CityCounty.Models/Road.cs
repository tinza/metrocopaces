﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.WindowsAzure.Mobile.Service;

namespace Copaces.CityCounty.Models
{
    public class Road : EntityData
    {
        public int? BridgeNum { get; set; }

        public double? BridgeWidth { get; set; }

        public int? CulvertAndPipeNum { get; set; }

        public String District { get; set; }

        public String From { get; set; }

        public String FunctionalClass { get; set; }

        public bool HasUnfinishedSurvey { get; set; }

        public String Jurisdiction { get; set; }

        public int? LaneNum { get; set; }

        public DateTime? LastSurveyDate { get; set; }

        public String Name { get; set; }

        public String PavMarkingCondition { get; set; }

        public double? PavWidthMax { get; set; }

        public double? PavWidthMin { get; set; }

        public double? PavWidthTypical { get; set; }

        // Place the road belongs to
        [ForeignKey("Place")]
        public String PlaceId { get; set; }

        public String PlaceType { get; set; }

        public String Remarks { get; set; }

        public double? ShoulderWidthMax { get; set; }

        public double? ShoulderWidthMin { get; set; }

        public double? ShoulderWidthTypical { get; set; }

        public String SurfaceType { get; set; }

        public String To { get; set; }

        public double? UnpavedShoulderWidth { get; set; }

        // Surveies
        public List<RoadSurvey> RoadSurveys { get; set; }

        public List<SegmentSurvey> SegmentSurveys { get; set; }

        public Place Place { get; set; }
    }
}