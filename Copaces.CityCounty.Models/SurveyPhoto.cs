﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Mobile.Service;

namespace Copaces.CityCounty.Models
{
    public class SurveyPhoto : EntityData
    {
        public string resourceName { get; set; }
        public string imageUri { get; set; }

        // The image location
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTime? TimeTaken { get; set; }

        public virtual SegmentSurvey SegmentSurvey { get; set; }
        [ForeignKey("SegmentSurvey")]
        public string SegmentSurveyId { get; set; }
    }
}