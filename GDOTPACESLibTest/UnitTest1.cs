﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using GDOTPACESLib;

namespace GDOTPACESLibTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var reader = new ConfigFileReader();
            var testRes = reader.GetBleedingDistress(1, 12);
            Assert.AreEqual(testRes, 5);
        }
    }
}
