﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Windows.Devices.Geolocation;
using Windows.Media.Capture;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Bing.Maps;
using MetroCOPACESGrid.Common;
using MetroCOPACESGrid.DataModel;
using MetroCOPACESGrid.UserControls;
using MetroCOPACESGrid.Utility;
using MetroCOPACESGrid.ViewModels;
using WinRTXamlToolkit.Controls.Extensions;
using WinRTXamlToolkit.Tools;

// The Split Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234234

namespace MetroCOPACESGrid
{
    public class NewSurveyPageNavParameter
    {
        public RoadSurvey RoadSurvey { get; set; }

        public List<Segment> Segments { get; set; }
    }

    /// <summary>
    ///     A page that displays a group title, a list of items within the group, and details for
    ///     the currently selected item.
    /// </summary>
    public sealed partial class NewSurveyPage : Page
    {
        private FlipView flipView;
        private Map map;
        private readonly NewSurveyPageVm defaultViewModel = new NewSurveyPageVm();

        private readonly Geolocator geoLocator = new Geolocator
        {
            DesiredAccuracy = PositionAccuracy.High
        };

        private readonly NavigationHelper navigationHelper;

        public NewSurveyPage()
        {
            InitializeComponent();

            // Add a handler for the map update event
            // SingleSegmentNewSurveyVm.UpdateGeopositionOnMap += UpdateMap;

            // Setup the navigation helper
            navigationHelper = new NavigationHelper(this);
            navigationHelper.LoadState += navigationHelper_LoadState;
            navigationHelper.SaveState += navigationHelper_SaveState;

            // Setup the logical page navigation components that allow
            // the page to only show one pane at a time.
            navigationHelper.GoBackCommand = new RelayCommand(() => GoBack(), () => CanGoBack());
            SegmentListView.SelectionChanged += SegmentListView_SelectionChanged;

            // Start listening for Window size changes 
            // to change from showing two panes to showing a single pane
            Window.Current.SizeChanged += Window_SizeChanged;
            InvalidateVisualState();
        }

        public NewSurveyPageVm DefaultViewModel { get { return defaultViewModel; } }

        public NavigationHelper NavigationHelper { get { return navigationHelper; } }

        private void SegmentListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DefaultViewModel.CurrentSegmentIndex = SegmentListView.SelectedIndex;
            if (DefaultViewModel.CurrentSegmentNewSurveyVm.IsSurveyEmpty)
            {
                // check if the checkboxes are checked
                if (GlobalPreferences.CopyFromLastSegment == true)
                    CopyFromLastSegment();
                if (GlobalPreferences.CopyFromLastSurvey == true)
                    CopyFromLastSurvey();
            }

            UpdateFlipView();

            if (UsingLogicalPageNavigation())
            {
                navigationHelper.GoBackCommand.RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        ///     To load data when the page starts
        /// </summary>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            if (e.PageState == null)
            {
                var param = e.NavigationParameter as NewSurveyPageNavParameter;
                DefaultViewModel.RoadSurvey = param.RoadSurvey;
                DefaultViewModel.Segments = param.Segments;

                CheckBoxCopyFromLastSegment.IsChecked = GlobalPreferences.CopyFromLastSegment;
                CheckBoxCopyFromLastSurvey.IsChecked = GlobalPreferences.CopyFromLastSurvey;

                if (!UsingLogicalPageNavigation() && SegmentViewSource.View != null)
                {
                    SegmentViewSource.View.MoveCurrentToFirst();
                }
            }
            else
            {
                if (e.PageState.ContainsKey("SelectedItem") && SegmentViewSource.View != null)
                {
                    SegmentViewSource.View.MoveCurrentTo(e.PageState["SelectedItem"]);
                }
            }
        }

        /// <summary>
        ///     To save data when the page exits.
        /// </summary>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            
        }

        /// <summary>
        ///     The callback function of the save button
        /// </summary>
        private async void ButtonSaveCurrentSegmentSurvey_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckDataValidation();
            }
            catch (Exception exception)
            {
                NotificationHelper.ShowErrorMessage(exception.Message);
                return;
            }
            await DefaultViewModel.CurrentSegmentNewSurveyVm.Save();
            NotificationHelper.ShowTwoLineToast("Save Segment Survey", "Segment survey data saved!");
        }

        /// <summary>
        ///     Callback function of the "Take Photo" button
        /// </summary>
        private async void ButtonTakePhoto_Click(object sender, RoutedEventArgs e)
        {
            var cameraCapture = new CameraCaptureUI();
            var photoStorageFile = await cameraCapture.CaptureFileAsync(CameraCaptureUIMode.Photo);

            if (photoStorageFile == null)
                return;
            await photoStorageFile.RenameAsync(DateTime.Now.ToFileTime() + ".jpg");

            var newSurveyPhotoWrapper = new SurveyPhotoWrapper(photoStorageFile);

            // Get the corresponding location of the photo taken
            try
            {
                if (!new[] { PositionStatus.NoData, PositionStatus.Disabled }.Contains(geoLocator.LocationStatus))
                {
                    newSurveyPhotoWrapper.Geoposition = await geoLocator.GetGeopositionAsync(
                        TimeSpan.FromMinutes(10), // The earliest acceptable location (from now)
                        TimeSpan.FromSeconds(5)); // Timeout limit
                }
                else
                {
                    NotificationHelper.ShowTwoLineToast("Error", "Location service is not available!");
                }
            }
            catch (Exception exception)
            {
                new MessageDialog(exception.Message).ShowAsync();
            }
            finally
            {
                DefaultViewModel.CurrentSegmentNewSurveyVm.SurveyPhotoWrappers.Add(newSurveyPhotoWrapper);
                UpdateFlipView();
            }
        }

        public void UpdateFlipView()
        {
            if (flipView == null)
                return;

            if (!DefaultViewModel.CurrentSegmentNewSurveyVm.SurveyPhotoWrappers.Any())
            {
                flipView.IsEnabled = false;
                flipView.ItemsSource = null;
            }
            else
            {
                flipView.IsEnabled = true;
                // Update the flipview
                flipView.ItemsSource =
                    from surveyPhoto in DefaultViewModel.CurrentSegmentNewSurveyVm.SurveyPhotoWrappers
                    select surveyPhoto;
            }
        }

        public async void UpdateMap(Geoposition pos)
        {
            if (map == null)
                return;

            if (map.Children.Count > 0)
            {
                map.Children.RemoveAt(0);
            }

            if (pos == null) return;

            try
            {
                var location = new Location(pos.Coordinate.Point.Position.Latitude,
                    pos.Coordinate.Point.Position.Longitude);
                var zoomLevel = 13.0f;

                // if we have GPS level accuracy
                if (pos.Coordinate.Accuracy <= 10)
                {
                    var icon = new LocationIcon10m();
                    // Add the 10m icon and zoom closer.
                    map.Children.Add(icon);
                    MapLayer.SetPosition(icon, location);
                    zoomLevel = 15.0f;
                }
                // Else if we have Wi-Fi level accuracy.
                else if (pos.Coordinate.Accuracy <= 100)
                {
                    var icon = new LocationIcon100m();
                    // Add the 100m icon and zoom a little closer.
                    map.Children.Add(icon);
                    MapLayer.SetPosition(icon, location);
                    zoomLevel = 14.0f;
                }
                else
                {
                    var icon = new LocationIcon500m();
                    // Add the 100m icon and zoom a little closer.
                    map.Children.Add(icon);
                    MapLayer.SetPosition(icon, location);
                    zoomLevel = 14.0f;
                }

                // Set the map to the given location and zoom level.
                map.SetView(location, zoomLevel);
            }
            catch (Exception exception)
            {
                new MessageDialog(exception.Message).ShowAsync();
            }
        }

        private void FlipView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selectedPhoto = ((FlipView)sender).SelectedItem as SurveyPhotoWrapper;
                UpdateMap(selectedPhoto != null ? selectedPhoto.Geoposition : null);
            }
            catch (Exception exception)
            {
                new MessageDialog(exception.Message).ShowAsync();
                throw;
            }
        }

        /// <summary>
        ///     A workaround to get access to the map control
        /// </summary>
        private void Map_OnLoaded(object sender, RoutedEventArgs e)
        {
            map = sender as Map;
        }

        /// <summary>
        ///     A workaround to get access to the flipview control
        /// </summary>
        private void FlipView_OnLoaded(object sender, RoutedEventArgs e)
        {
            flipView = sender as FlipView;
            UpdateFlipView();
        }

        #region Logical page navigation

        // The split page isdesigned so that when the Window does have enough space to show
        // both the list and the dteails, only one pane will be shown at at time.
        //
        // This is all implemented with a single physical page that can represent two logical
        // pages.  The code below achieves this goal without making the user aware of the
        // distinction.

        private const int MinimumWidthForSupportingTwoPanes = 768;

        /// <summary>
        ///     Invoked to determine whether the page should act as one logical page or two.
        /// </summary>
        /// <returns>
        ///     True if the window should show act as one logical page, false
        ///     otherwise.
        /// </returns>
        private bool UsingLogicalPageNavigation()
        {
            return Window.Current.Bounds.Width < MinimumWidthForSupportingTwoPanes;
        }

        /// <summary>
        ///     Invoked with the Window changes size
        /// </summary>
        /// <param name="sender">The current Window</param>
        /// <param name="e">Event data that describes the new size of the Window</param>
        private void Window_SizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            InvalidateVisualState();
        }

        private bool CanGoBack()
        {
            if (UsingLogicalPageNavigation() && SegmentListView.SelectedItem != null)
            {
                return true;
            }
            return navigationHelper.CanGoBack();
        }

        private void GoBack()
        {
            if (UsingLogicalPageNavigation() && SegmentListView.SelectedItem != null)
            {
                // When logical page navigation is in effect and there's a selected item that
                // item's details are currently displayed.  Clearing the selection will return to
                // the item list.  From the user's point of view this is a logical backward
                // navigation.
                SegmentListView.SelectedItem = null;
            }
            else
            {
                navigationHelper.GoBack();
            }
        }

        private void InvalidateVisualState()
        {
            var visualState = DetermineVisualState();
            VisualStateManager.GoToState(this, visualState, false);
            navigationHelper.GoBackCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        ///     Invoked to determine the name of the visual state that corresponds to an application
        ///     view state.
        /// </summary>
        /// <returns>
        ///     The name of the desired visual state.  This is the same as the name of the
        ///     view state except when there is a selected item in portrait and snapped views where
        ///     this additional logical page is represented by adding a suffix of _Detail.
        /// </returns>
        private string DetermineVisualState()
        {
            if (!UsingLogicalPageNavigation())
                return "PrimaryView";

            // Update the back button's enabled state when the view state changes
            var logicalPageBack = UsingLogicalPageNavigation() && SegmentListView.SelectedItem != null;

            return logicalPageBack ? "SinglePane_Detail" : "SinglePane";
        }

        #endregion

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        void CheckDataValidation()
        {
            // Check individual textboxes
            var isIndividualValid = FindVisualChildren<SegmentSurveyTextBox>(PageRoot).All(tb => tb.IsDataValid);
            if (!isIndividualValid)
            {
                throw new Exception("Individual input data errr found. Please recheck.");
            }

            // Check the sum of 4 load levels
            var survey = DefaultViewModel.CurrentSegmentNewSurveyVm.Survey;
            if ((survey.LoadLevel1 + survey.LoadLevel2 + survey.LoadLevel3 + survey.LoadLevel4) > 100)
            {
                throw new Exception("Sum of all Load Cracking values should be less than or equal to 100");
            }

            // Check combos of percentage and severity
            Func<object, object, bool> IsOnlyOneGiven = (obj1, obj2) => (obj1 != null && obj2 == null) || (obj1 == null && obj2 != null);
            if (IsOnlyOneGiven(survey.BlockLevel, survey.BlockPercentage) ||
                IsOnlyOneGiven(survey.RavelLevel, survey.RavelPercentage) ||
                IsOnlyOneGiven(survey.EdgeLevel, survey.EdgePercentage) ||
                IsOnlyOneGiven(survey.BleedLevel, survey.BleedPercentage) ||
                IsOnlyOneGiven(survey.LossPavLevel, survey.LossPavPercentage) ||
                IsOnlyOneGiven(survey.CorrugLevel, survey.CorrugPercentage))
            {
                throw new Exception("Distress severity level and percentage should be provided together.");
            }
        }

        /// <summary>
        /// Recursively find all the controls of type T in the page
        /// </summary>
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        private async void CopyFromLastSurvey()
        {
            var survey = DefaultViewModel.CurrentSegmentNewSurveyVm.Survey;
            var segment = DefaultViewModel.CurrentSegmentNewSurveyVm.Segment;
            var lastSurvey = await SyncHelper.GetLatestSegmentSurveyBySegmentInventId(segment.InventId);

            if (lastSurvey != null)
            {
                lastSurvey.RoadSurveyDate = survey.RoadSurveyDate;
                lastSurvey.RoadSurveyId = survey.RoadSurveyId;
                lastSurvey.SegSurveyDate = survey.SegSurveyDate;

                DefaultViewModel.CurrentSegmentNewSurveyVm.Survey = lastSurvey;
            }
        }

        private void CopyFromLastSegment()
        {
            if (DefaultViewModel.CurrentSegmentIndex > 0)
            {
                var newSurvey = new SegmentSurvey();
                var oriSurvey = DefaultViewModel.CurrentSegmentNewSurveyVm.Survey;
                var lastSurvey = DefaultViewModel.NewSurveyVms[DefaultViewModel.CurrentSegmentIndex - 1].Survey;

                foreach (var propInfo in lastSurvey.GetType().GetTypeInfo().DeclaredProperties)
                {
                    var value = propInfo.GetValue(lastSurvey);
                    propInfo.SetValue(newSurvey, value);
                }

                newSurvey.RoadSurveyDate = oriSurvey.RoadSurveyDate;
                newSurvey.RoadSurveyId = oriSurvey.RoadSurveyId;
                newSurvey.SegSurveyDate = oriSurvey.SegSurveyDate;

                DefaultViewModel.CurrentSegmentNewSurveyVm.Survey = newSurvey;
            }
        }

        private async void ButtonGoBack_Click(object sender, RoutedEventArgs e)
        {
            var rootFrame = Window.Current.Content as Frame;

            // Determine if the current segment survey is saved yet
            if (DefaultViewModel.CurrentSegmentNewSurveyVm.EnableSaveButton)
            {
                var dialog =
                    new MessageDialog(
                        "The current survey content is not saved yet, do you want to discard the content and leave?");
                dialog.Commands.Add(new UICommand("Yes"));
                dialog.Commands.Add(new UICommand("No, I'll stay"));
                dialog.DefaultCommandIndex = 1;

                var result = await dialog.ShowAsync();
                if (result.Label == "No, I'll stay")
                    return;
            }

            if (rootFrame.CanGoBack)
                Frame.GoBack();
        }

        private void CheckBoxCopyFromLastSegment_OnChecked(object sender, RoutedEventArgs e)
        {
            var cbx = sender as CheckBox;
            GlobalPreferences.CopyFromLastSegment = cbx.IsChecked == true;
        }

        private void CheckBoxCopyFromLastSurvey_OnChecked(object sender, RoutedEventArgs e)
        {
            var cbx = sender as CheckBox;
            GlobalPreferences.CopyFromLastSurvey = cbx.IsChecked == true;
        }
    }
}