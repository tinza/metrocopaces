﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI.Popups;
using MetroCOPACESGrid.DataModel;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace MetroCOPACESGrid.Utility
{
    public class SyncHelper
    {
        public static async Task AddNewRoadSurvey(RoadSurvey survey, Road road = null)
        {
            await App.LocalRoadSurveyTable.InsertAsync(survey);
            if (road == null)
                road = await App.LocalRoadTable.LookupAsync(survey.RoadId);

            await road.AddNewSurvey(survey);
        }

        /// <summary>
        ///     Check if the current machine has internet access
        /// </summary>
        /// <returns></returns>
        public static bool CheckForInternetConnection()
        {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile == null) return false;
            return profile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
        }

        public static async Task<bool> CheckPlaceDataExist()
        {
            var query = await App.LocalPlaceTable.Take(1).ToListAsync();
            return query != null && query.Count != 0;
        }

        /// <summary>
        ///     Check if the locally cached road data exist
        /// </summary>
        public static async Task<bool> CheckRoadDataExist()
        {
            var localRoadCountQuery = await App.LocalRoadTable.Take(10).ToListAsync();
            return localRoadCountQuery != null && localRoadCountQuery.Count != 0;
        }

        /// <summary>
        ///     Check if the locally cached segment data exist
        /// </summary>
        public static async Task<bool> CheckSegmentDataExist()
        {
            var localSegCountQuery = await App.LocalSegmentTable.Take(10).ToListAsync();
            return localSegCountQuery != null && localSegCountQuery.Count != 0;
        }

        public static async Task<List<Road>> GetLocalRoads(int skip = 0, int take = 100)
        {
            var roadQuery = App.LocalRoadTable
                .Skip(skip)
                .Take(take).OrderBy(r => r.Name).ToListAsync();
            return await roadQuery;
        }

        /// <summary>
        ///     Get RoadSurveys assosiated with a given road
        /// </summary>
        public static async Task<List<RoadSurvey>> GetRoadSurveysByRoadId(string roadId)
        {
            var query = await App.LocalRoadSurveyTable
                .Where(rs => rs.RoadId == roadId)
                .OrderByDescending(rs => rs.Date)
                .ToListAsync();
            return query ?? new List<RoadSurvey>();
        }

        public static async Task<List<Road>> SearchRoadsByName(string searchString,
            bool unfinishedSurveyOnly = false, DateTime? lastSurveyTimeFilter = null)
        {
            var query = App.LocalRoadTable
                .Where(item => item.Name.ToLower().StartsWith(searchString.ToLower()));
            if (unfinishedSurveyOnly)
                query = query.Where(road => road.HasUnfinishedSurvey);
            if (lastSurveyTimeFilter.HasValue)
                query = query.Where(road => road.LastSurveyDate >= lastSurveyTimeFilter);
            return await query.OrderBy(r => r.Name).ToListAsync();
        }

        public static async Task<RoadSurvey> GetLatestRoadSurveyByRoadId(string roadId)
        {
            return (await GetRoadSurveysByRoadId(roadId)).FirstOrDefault();
        }

        public static async Task<SegmentSurvey> GetLatestSegmentSurveyBySegmentInventId(int inventId)
        {
            var surveys = await App.LocalSegmentSurveyTable
                .Where(ss => ss.InventId == inventId)
                .OrderByDescending(ss => ss.SegSurveyDate)
                .Take(1).ToListAsync();
            return surveys.FirstOrDefault();
        }

        public static async Task<Segment> GetSegmentByInventId(int inventId)
        {
            return (await App.LocalSegmentTable.Where(s => s.InventId == inventId).ToEnumerableAsync()).FirstOrDefault();
        }

        public static async Task<List<Segment>> GetSegmentsByIds(List<int> inventIds)
        {
            return await App.LocalSegmentTable
                .Where(s => inventIds.Contains(s.InventId))
                .ToListAsync();
        }

        public static async Task<List<Segment>> GetSegmentsByRoadId(string roadId)
        {
            return await App.LocalSegmentTable
                .Where(seg => seg.RoadId == roadId)
                .OrderBy(seg => seg.SequenceId)
                .LoadAllAsync();
        }

        public static async Task<List<SegmentSurvey>> GetSegmentSurveysByRoadSurvey(RoadSurvey roadSurvey)
        {
            return await App.LocalSegmentSurveyTable
                .Where(ss => ss.RoadSurveyId == roadSurvey.Id)
                .ToListAsync();
        }

        /// <summary>
        ///     Initialize the sync context
        /// </summary>
        public static async Task InitializeSyncContext()
        {
            if (!App.MobileService.SyncContext.IsInitialized)
            {
                var store = new MobileServiceSQLiteStore("local.db");
                store.DefineTable<Place>();
                store.DefineTable<Road>();
                store.DefineTable<Segment>();
                store.DefineTable<SegmentSurvey>();
                store.DefineTable<RoadSurvey>();
                store.DefineTable<SurveyPhoto>();
                await App.MobileService.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());
            }
        }

        public static async Task Sync()
        {
            try
            {
                if (!CheckForInternetConnection())
                {
                    await new MessageDialog("No internet connection. Please try again later.").ShowAsync();
                    return;
                }

                var curPlaceId = DbHelper.Place.Id;
                if (App.MobileService.SyncContext.PendingOperations != 0)
                {
                    await App.MobileService.SyncContext.PushAsync();
                }

                await UploadSurveyImages();

                if (!await CheckSegmentDataExist())
                {
                    var query = App.LocalSegmentTable.Where(s => s.PlaceId == curPlaceId);
                    await App.LocalSegmentTable.PullAsync(query);
                }
                if (!await CheckRoadDataExist())
                {
                    var query = App.LocalRoadTable.Where(r => r.PlaceId == curPlaceId);
                    await App.LocalRoadTable.PullAsync(query);
                }

                // Pull the road survey data
                // Kindof stupid because SQL azure doesn't support 'Join' as of 10/5/2014
                var roadSurveyQuery = App.LocalRoadSurveyTable.Where(rs => rs.PlaceId == curPlaceId);
                await App.LocalRoadSurveyTable.PullAsync(roadSurveyQuery);

                // Pull the segment survey data
                var segSurveyQuery = App.LocalSegmentSurveyTable.Where(ss => ss.PlaceId == curPlaceId);
                await App.LocalSegmentSurveyTable.PullAsync(segSurveyQuery);
                new MessageDialog("Sync completed!").ShowAsync();
            }
            catch (Exception exception)
            {
                new MessageDialog("Error:" + exception.Message).ShowAsync();
            }
        }

        public static async Task UpdateLocalSegmentSurvey(SegmentSurvey survey)
        {
            await App.LocalSegmentSurveyTable.UpdateAsync(survey);
        }

        /// <summary>
        ///     Upload the SurveyPhoto data to a SQL table, and also upload the corresponding image files to blob storage
        ///     assuming network access
        /// </summary>
        //public static async Task UploadImages(IMobileServiceTable<SurveyPhoto> table, IEnumerable<StorageFile> files)
        //{
        //    var sasQueryString = await App.MobileService.InvokeApiAsync<String>("FileUpload", HttpMethod.Get, null);

        //    foreach (var file in files)
        //    {
        //        var surveyPhoto = await ImagingHelper.ReadImageMetaData(file);
        //        // Insert the photo into Photo table, and obtain the generated ImageUrl
        //        await table.InsertAsync(surveyPhoto);

        //        // Get the URI generated that contains the SAS 
        //        // and extract the storage credentials.
        //        var cred = new StorageCredentials(sasQueryString);
        //        var imageUri = new Uri(surveyPhoto.ImageUri);

        //        // Instantiate a Blob store container based on the info in the returned item.
        //        var container = new CloudBlobContainer(
        //            new Uri(String.Format("https://{0}/{1}",
        //                imageUri.Host, "segmentsurvey-images")), cred);

        //        // Get the new image as a stream.
        //        using (var fileStream = await file.OpenStreamForReadAsync())
        //        {
        //            // Upload the new image as a BLOB from the stream.
        //            var blobFromSasCredential = container.GetBlockBlobReference(surveyPhoto.ResourceName);
        //            await blobFromSasCredential.UploadFromStreamAsync(fileStream.AsInputStream());
        //        }
        //    }
        //}

        /// <summary>
        ///     The function uploads a SegmentSurvey and
        ///     associated survey photos to the server
        /// </summary>
        public static async Task UploadSurveyImages()
        {
            var imageFolder =
                await ApplicationData.Current.LocalFolder.TryGetItemAsync("UnuploadedImages") as StorageFolder;
            if (imageFolder == null || (await imageFolder.GetFilesAsync()).Count == 0)
                return;

            var surveyPhotoTable = App.MobileService.GetTable<SurveyPhoto>();
            var images = (await imageFolder.GetFilesAsync()).Where(file => file.FileType == ".jpg").ToList();
            
            var sasQueryString = await App.MobileService.InvokeApiAsync<String>("FileUpload", HttpMethod.Get, null);
            foreach (var image in images)
            {
                var surveyPhoto = await ImagingHelper.ReadImageMetaData(image);
                // Insert the photo into Photo table, and obtain the generated ImageUrl
                await surveyPhotoTable.InsertAsync(surveyPhoto);

                // Get the URI generated that contains the SAS 
                // and extract the storage credentials.
                var cred = new StorageCredentials(sasQueryString);
                var imageUri = new Uri(surveyPhoto.ImageUri);

                // Instantiate a Blob store container based on the info in the returned item.
                var container = new CloudBlobContainer(
                    new Uri(String.Format("https://{0}/{1}",
                        imageUri.Host, "segmentsurvey-images")), cred);

                // Get the new image as a stream.
                using (var fileStream = await image.OpenStreamForReadAsync())
                {
                    // Upload the new image as a BLOB from the stream.
                    var blobFromSasCredential = container.GetBlockBlobReference(surveyPhoto.ResourceName);
                    await blobFromSasCredential.UploadFromStreamAsync(fileStream.AsInputStream());
                }
                await image.DeleteAsync();
            }
        }
    }
}