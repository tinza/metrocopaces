﻿using System;
using Windows.UI.Xaml.Data;

namespace MetroCOPACESGrid.Utility.Converters
{
    public class DoublePropertyBindingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value == null ? null : value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            double? res;
            try
            {
                res = System.Convert.ToDouble(value);
            }
            catch
            {
                res = null;
            }

            return res;
        }
    }
}
