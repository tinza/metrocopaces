﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Windows.UI.Xaml.Data;
using MetroCOPACESGrid.DataModel;

namespace MetroCOPACESGrid.Utility.Converters
{
    public class RoadSegmentPropertyConverter : IValueConverter
    {
        private static readonly string[] DisplayableRoadProperties = { "From", "To", "Id", "FunctionalClass", "SurfaceType", "LastSurveyDate" };
        private static readonly string[] DisplayableSegmentProperties = { "InventId", "From", "To", "Length", "Remarks" };

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            // If no specified parameter, return all Displayable properties
            if (parameter == null)
            {
                var propsToDisplay = value.GetType() == typeof(Road) ? DisplayableRoadProperties : DisplayableSegmentProperties;
                var props = value.GetType().GetTypeInfo().DeclaredProperties;

                var res = propsToDisplay.Select(
                    propName =>
                    {
                        var prop = props.First(p => p.Name == propName);
                        return new
                        {
                            Name = prop.IsDefined(typeof(DisplayAttribute))
                                ? ((DisplayAttribute)prop.GetCustomAttribute(typeof(DisplayAttribute))).Name
                                : prop.Name,
                            Value = prop.GetValue(value) != null 
                                ? prop.PropertyType == typeof(DateTime) 
                                    ? ((DateTime)prop.GetValue(value)).ToString("d")
                                    : prop.GetValue(value)
                                : "N/A"
                        };
                    }
                );

                return res;
            }
            // If specified parameter, return it's value
            else
            {
                var propName = parameter as String;
                var propValue = value.GetType().GetTypeInfo().GetDeclaredProperty(propName).GetValue(value) ?? "N/A";
                return propName + " : " + propValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
