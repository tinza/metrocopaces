﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using MetroCOPACESGrid.DataModel;

namespace MetroCOPACESGrid.Utility.Converters
{
    public class SegmentSurveyPropertyOptionsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var props = typeof(SegmentSurvey).GetTypeInfo().DeclaredProperties;
            var propName = parameter as String;
            var propertyInfos = props.ToList();
            if (propertyInfos.All(p => p.Name != propName))
                return null;

            var prop = propertyInfos.First(p => p.Name == propName);
            return AttributeHelper.GetOptions(prop);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
