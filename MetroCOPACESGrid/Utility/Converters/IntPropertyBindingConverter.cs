﻿using System;
using Windows.UI.Xaml.Data;

namespace MetroCOPACESGrid.Utility.Converters
{
    public class IntPropertyBindingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value == null ? null : value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            int? res;
            try
            {
                res = System.Convert.ToInt32(value);
            }
            catch
            {
                res = null;
            }

            return res;
        }
    }
}
