﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Notifications;

namespace MetroCOPACESGrid.Utility
{
    public static class NotificationHelper
    {
        public static void ShowTwoLineToast(string title, string content)
        {
            var toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);
            var toastTextContent = toastXml.GetElementsByTagName("text");
            toastTextContent[0].AppendChild(toastXml.CreateTextNode(title));
            toastTextContent[1].AppendChild(toastXml.CreateTextNode(content));
            ToastNotificationManager.CreateToastNotifier().Show(new ToastNotification(toastXml));
        }

        public static void ShowErrorMessage(string message)
        {
            var toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);
            var toastTextContent = toastXml.GetElementsByTagName("text");
            toastTextContent[0].AppendChild(toastXml.CreateTextNode("Error"));
            toastTextContent[1].AppendChild(toastXml.CreateTextNode(message));
            ToastNotificationManager.CreateToastNotifier().Show(new ToastNotification(toastXml));
        }
    }
}
