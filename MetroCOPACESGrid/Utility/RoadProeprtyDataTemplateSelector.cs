﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MetroCOPACESGrid.DataModel;

namespace MetroCOPACESGrid.Utility
{
    public class RoadProeprtyDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TextField { get; set; }
        public DataTemplate ComboBoxField { get; set; }

        protected override DataTemplate SelectTemplateCore(dynamic item)
        {
            if (item.Options != null)
                Debug.WriteLine("No");
            return item.Options == null ? TextField : ComboBoxField;
        }
    }
}
