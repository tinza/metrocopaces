﻿using System.Runtime.InteropServices;
using System.Threading.Tasks;
using MetroCOPACESGrid.Common;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MetroCOPACESGrid.Utility;
using MetroCOPACESGrid.DataModel;
using Windows.UI.Popups;

using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Newtonsoft.Json.Linq;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MetroCOPACESGrid
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PlaceSelectPage : Page
    {
        private ObservableDictionary viewModel = new ObservableDictionary();
        public ObservableDictionary ViewModel { get { return viewModel; } }

        private readonly IMobileServiceSyncTable<Place> placeTable = App.MobileService.GetSyncTable<Place>();

        public PlaceSelectPage()
        {
            // Initilize the dictionary fields
            ViewModel["Type"] = "Type";
            // The current selected place list
            ViewModel["PlaceList"] = new List<Place>();
            // The list for all counties
            ViewModel["CountyList"] = new List<Place>();
            // The list for all cities
            ViewModel["CityList"] = new List<Place>();
            ViewModel["SelectedPlace"] = null;

            NavigationCacheMode = NavigationCacheMode.Enabled;
            InitializeComponent();
        }

        private void OnTypeFlyoutClick(object sender, RoutedEventArgs e)
        {
            var item = (MenuFlyoutItem)sender;
            var selectedValue = item.Text;
            if ((String)ViewModel["Type"] == selectedValue) return;

            ViewModel["Type"] = selectedValue;
            UpdatePlaceNameList(selectedValue);

            if (ViewModel.ContainsKey("SelectedPlace"))
                ViewModel.Remove("SelectedPlace");
        }

        private void UpdatePlaceNameList(String type)
        {
            if (ViewModel.ContainsKey(type + "List"))
                ViewModel["PlaceList"] = ViewModel[type + "List"];
        }

        /// <summary>
        /// Fill the place name list combobox
        /// </summary>
        private async Task LoadPlaceNameLists()
        {
            // Temp: Use NEWTON county for test
            ViewModel["CountyList"] = await placeTable
                .Where(item => item.Type == "County")
                .OrderBy(item => item.Name).LoadAllAsync();
            ViewModel["CityList"] = await placeTable
                .Where(item => item.Type == "City")
                .OrderBy(item => item.Name).LoadAllAsync();
        }

        /// <summary>
        /// Callback function for the "Select" button
        /// </summary>
        private async void ButtonSelect_OnClick(object sender, RoutedEventArgs e)
        {
            // Validate selection
            if (!ViewModel.ContainsKey("SelectedPlace") || ViewModel["SelectedPlace"] == null)
            {
                await new MessageDialog("Please select a survey place to process.").ShowAsync();
                return;
            }

            Windows.Storage.ApplicationData.Current.LocalSettings.Values["SavedType"] = ViewModel["Type"];
            var selectedPlace = ViewModel["SelectedPlace"] as Place;
            Windows.Storage.ApplicationData.Current.LocalSettings.Values["SavedPlaceId"] = selectedPlace.Id;
            DbHelper.Place = selectedPlace;
            Frame.Navigate(typeof(MainPage));
        }

        private void ComboBoxPlace_OnSelectionChange(object sender, SelectionChangedEventArgs e)
        {
            ViewModel["SelectedPlace"] = ((ComboBox)sender).SelectedItem as Place;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            await SyncHelper.InitializeSyncContext();
            if (!await SyncHelper.CheckPlaceDataExist())
            {
                if (!SyncHelper.CheckForInternetConnection())
                {
                    new MessageDialog(
                        "Cannot find local data, and there appears no Internet connection. Please download data when connected.")
                        .ShowAsync();
                    return;
                }

                // If network connection is on, then download the data
                try
                {
                    // There is no cached version, so pull from the server
                    ProgressDialog.IsOpen = true;
                    var query = App.RemotePlaceTable.Where(p => p.Id != null);
                    await placeTable.PullAsync(query);
                    ProgressDialog.IsOpen = false;
                }
                catch (Exception exception)
                {
                    new MessageDialog("Error: " + exception.Message).ShowAsync();
                    return;
                }
            }

            await LoadPlaceNameLists();

            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings.Values;
            if (localSettings.ContainsKey("SavedType") && localSettings.ContainsKey("SavedPlaceId"))
            {
                var savedType = localSettings["SavedType"] as String;
                var savedPlaceId = localSettings["SavedPlaceId"] as String;
                ViewModel["Type"] = savedType;
                var curPlaceList = ViewModel[savedType + "List"] as IEnumerable<Place>;
                ViewModel["PlaceList"] = curPlaceList;

                if (curPlaceList.Any(p => p.Id == savedPlaceId))
                    ViewModel["SelectedPlace"] = curPlaceList.First(p => p.Id == savedPlaceId);
            }
        }
    }
}
