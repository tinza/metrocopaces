﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MetroCOPACESGrid.Common;
using MetroCOPACESGrid.DataModel;
using MetroCOPACESGrid.UserControls;
using MetroCOPACESGrid.Utility;

// The Split Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234234

namespace MetroCOPACESGrid
{
    public class EditSurveyPageNavParameter
    {
        public List<SegmentSurvey> SegmentSurveys { get; set; }
        public RoadSurvey RoadSurvey { get; set; }
    }

    public class EditSurveyPageViewModel : ObservableDictionary
    {
        public EditSurveyPageViewModel()
        {
            this["SegmentSurveys"] = null;
            this["RoadSurvey"] = null;
            this["Segments"] = null;
            this["CurrentSegmentSurveyIsWindShield"] = false;
        }

        public List<SegmentSurvey> SegmentSurveys
        {
            get { return this["SegmentSurveys"] as List<SegmentSurvey>; }
            set
            {
                this["SegmentSurveys"] = value;
                FindSegments();
            }
        }

        public RoadSurvey RoadSurvey
        {
            get { return this["RoadSurvey"] as RoadSurvey; }
            set { this["RoadSurvey"] = value; }
        }

        public List<Segment> Segments
        {
            get { return this["Segments"] as List<Segment>; }
            set { this["Segments"] = value; }
        }

        public SegmentSurvey CurrentSegmentSurvey
        {
            get { return this["CurrentSegmentSurvey"] as SegmentSurvey; }
            set
            {
                this["CurrentSegmentSurvey"] = value;
                CurrentSegmentSurveyIsWindShield = value.IsWindshieldSurvey;
            }
        }

        // Seperate the property because it needs raise NotifyPropertyChanged event
        // to update UI
        public bool CurrentSegmentSurveyIsWindShield
        {
            get { return (bool)this["CurrentSegmentSurveyIsWindShield"]; }
            set
            {
                this["CurrentSegmentSurveyIsWindShield"] = value;
                if (CurrentSegmentSurvey.IsWindshieldSurvey != value)
                    CurrentSegmentSurvey.IsWindshieldSurvey = value;
            }
        }

        public Segment CurrentSegment
        {
            get { return this["CurrentSegment"] as Segment; }
            set { this["CurrentSegment"] = value; }
        }

        private int currentIndex;
        public int CurrentIndex
        {
            get { return currentIndex; }
            set
            {
                currentIndex = value;
                CurrentSegmentSurvey = SegmentSurveys[value];
                CurrentSegment = Segments[value];
            }
        }

        public async void FindSegments()
        {
            var segmentList = new List<Segment>();
            foreach (var segmentSurvey in SegmentSurveys)
            {
                segmentList.Add(await segmentSurvey.GetSegment());
            }
            Segments = segmentList;
        }


    }

    /// <summary>
    /// Edit the conducted survey
    /// </summary>
    public sealed partial class EditSurveyPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        public NavigationHelper NavigationHelper
        {
            get { return navigationHelper; }
        }

        private EditSurveyPageViewModel viewModel = new EditSurveyPageViewModel();
        public EditSurveyPageViewModel ViewModel { get { return viewModel; } }

        public EditSurveyPage()
        {
            InitializeComponent();

            // Add a handler for the map update event
            // SingleSegmentNewSurveyVm.UpdateGeopositionOnMap += UpdateMap;

            // Setup the navigation helper
            navigationHelper = new NavigationHelper(this);
            navigationHelper.LoadState += navigationHelper_LoadState;
            navigationHelper.SaveState += navigationHelper_SaveState;

            // Setup the logical page navigation components that allow
            // the page to only show one pane at a time.
            navigationHelper.GoBackCommand = new RelayCommand(() => GoBack(), () => CanGoBack());

            // Start listening for Window size changes 
            // to change from showing two panes to showing a single pane
            Window.Current.SizeChanged += Window_SizeChanged;
            InvalidateVisualState();
        }

        /// <summary>
        /// To load data when the page starts
        /// </summary>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            if (e.PageState == null)
            {
                var navParameter = e.NavigationParameter as EditSurveyPageNavParameter;
                ViewModel.SegmentSurveys = navParameter.SegmentSurveys;
                ViewModel.RoadSurvey = navParameter.RoadSurvey;
            }
        }

        /// <summary>
        /// To save data when the page exits.
        /// </summary>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region Logical page navigation

        // The split page isdesigned so that when the Window does have enough space to show
        // both the list and the dteails, only one pane will be shown at at time.
        //
        // This is all implemented with a single physical page that can represent two logical
        // pages.  The code below achieves this goal without making the user aware of the
        // distinction.

        private const int MinimumWidthForSupportingTwoPanes = 768;

        /// <summary>
        /// Invoked to determine whether the page should act as one logical page or two.
        /// </summary>
        /// <returns>True if the window should show act as one logical page, false
        /// otherwise.</returns>
        private bool UsingLogicalPageNavigation()
        {
            return Window.Current.Bounds.Width < MinimumWidthForSupportingTwoPanes;
        }

        /// <summary>
        /// Invoked with the Window changes size
        /// </summary>
        /// <param name="sender">The current Window</param>
        /// <param name="e">Event data that describes the new size of the Window</param>
        private void Window_SizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            InvalidateVisualState();
        }
        private bool CanGoBack()
        {
            if (UsingLogicalPageNavigation() && SegmentListView.SelectedItem != null)
            {
                return true;
            }
            return navigationHelper.CanGoBack();
        }

        private void GoBack()
        {
            if (UsingLogicalPageNavigation() && SegmentListView.SelectedItem != null)
            {
                // When logical page navigation is in effect and there's a selected item that
                // item's details are currently displayed.  Clearing the selection will return to
                // the item list.  From the user's point of view this is a logical backward
                // navigation.
                SegmentListView.SelectedItem = null;
            }
            else
            {
                navigationHelper.GoBack();
            }
        }

        private void InvalidateVisualState()
        {
            var visualState = DetermineVisualState();
            VisualStateManager.GoToState(this, visualState, false);
            navigationHelper.GoBackCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Invoked to determine the name of the visual state that corresponds to an application
        /// view state.
        /// </summary>
        /// <returns>The name of the desired visual state.  This is the same as the name of the
        /// view state except when there is a selected item in portrait and snapped views where
        /// this additional logical page is represented by adding a suffix of _Detail.</returns>
        private string DetermineVisualState()
        {
            if (!UsingLogicalPageNavigation())
                return "PrimaryView";

            // Update the back button's enabled state when the view state changes
            var logicalPageBack = UsingLogicalPageNavigation() && SegmentListView.SelectedItem != null;

            return logicalPageBack ? "SinglePane_Detail" : "SinglePane";
        }

        #endregion

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="Common.NavigationHelper.LoadState"/>
        /// and <see cref="Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void SegmentListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.CurrentIndex = SegmentListView.SelectedIndex;
        }

        private async void ButtonUpdateSurvey_OnClick(object sender, RoutedEventArgs e)
        {
            var item = ViewModel.CurrentSegmentSurvey;
            if (item != null)
            {
                try
                {
                    CheckDataValidation();
                }
                catch (Exception exception)
                {
                    NotificationHelper.ShowErrorMessage(exception.Message);
                    return;
                }

                await SyncHelper.UpdateLocalSegmentSurvey(item);
                NotificationHelper.ShowTwoLineToast("Update Segment Survey", "Update completed!");
            }
        }

        void CheckDataValidation()
        {
            // Check individual textboxes
            var isIndividualValid = FindVisualChildren<SegmentSurveyTextBox>(PageRoot).All(tb => tb.IsDataValid);
            if (!isIndividualValid)
            {
                throw new Exception("Individual input data errr found. Please recheck.");
            }

            // Check the sum of 4 load levels
            var survey = ViewModel.CurrentSegmentSurvey;
            if ((survey.LoadLevel1 + survey.LoadLevel2 + survey.LoadLevel3 + survey.LoadLevel4) > 100)
            {
                throw new Exception("Sum of all Load Cracking values should be less than or equal to 100");
            }

            // Check combos of percentage and severity
            Func<object, object, bool> IsOnlyOneGiven = (obj1, obj2) => (obj1 != null && obj2 == null) || (obj1 == null && obj2 != null);
            if (IsOnlyOneGiven(survey.BlockLevel, survey.BlockPercentage) ||
                IsOnlyOneGiven(survey.RavelLevel, survey.RavelPercentage) ||
                IsOnlyOneGiven(survey.EdgeLevel, survey.EdgePercentage) ||
                IsOnlyOneGiven(survey.BleedLevel, survey.BleedPercentage) ||
                IsOnlyOneGiven(survey.LossPavLevel, survey.LossPavPercentage) ||
                IsOnlyOneGiven(survey.CorrugLevel, survey.CorrugPercentage))
            {
                throw new Exception("Distress severity level and percentage should be provided together.");
            }
        }

        /// <summary>
        /// Recursively find all the controls of type T in the page
        /// </summary>
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
    }
}
