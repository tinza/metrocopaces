﻿using System;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace MetroCOPACESGrid.DataModel
{
    public class Segment
    {
        [JsonProperty(PropertyName = "from")]
        public String From { get; set; }

        [JsonProperty(PropertyName = "hasCurbOrGutter")]
        public String HasCurbOrGutter { get; set; }

        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "inventId")]
        public int InventId { get; set; }

        [JsonProperty(PropertyName = "laneDirection")]
        public String LaneDirection { get; set; }

        [JsonProperty(PropertyName = "laneNum")]
        public int? LaneNum { get; set; }

        [JsonProperty(PropertyName = "length")]
        public double? Length { get; set; }

        [JsonProperty(PropertyName = "placeId")]
        public String PlaceId { get; set; }

        [JsonProperty(PropertyName = "remarks")]
        public String Remarks { get; set; }

        [JsonProperty(PropertyName = "roadId")]
        public String RoadId { get; set; }

        [JsonProperty(PropertyName = "sampleLocation")]
        public String SampleLocation { get; set; }

        [JsonProperty(PropertyName = "sequenceId")]
        public int? SequenceId { get; set; }

        [JsonProperty(PropertyName = "to")]
        public String To { get; set; }

        [Version]
        public string Version { get; set; }

        public override string ToString()
        {
            return String.Format("{0,-10} {1,-20} {2,-20} {3,5}", SequenceId, From, To, Length);
        }
    }
}