using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace MetroCOPACESGrid.DataModel
{
    /// <summary>
    /// Properties that are not displayed on the UI
    /// </summary>
    class InternalAttribute : Attribute
    {
    }

    /// <summary>
    /// Properties that are not editable on the UI
    /// </summary>
    class ReadOnlyAttribute : Attribute
    {
    }

    /// <summary>
    /// Properties whose value can only set by selection
    /// </summary>
    class OptionsAttribute : Attribute
    {
        public List<String> Options { get; set; }

        public OptionsAttribute(params string[] options)
        {
            Options = options.ToList();
            Options.Add(" ");
        }

        public OptionsAttribute(OptionType type, params int[] param)
        {
            switch (type)
            {
                case OptionType.Severity:
                    var maxSeverity = param[0];
                    Options = Enumerable.Range(1, maxSeverity).Select(e => e.ToString()).ToList();
                    break;
                case OptionType.DateYear:
                    var curYear = DateTime.Now.Year;
                    var numYear = param[0];
                    Options = Enumerable.Range(curYear - numYear + 1, numYear).Select(e => e.ToString()).ToList();
                    break;
                case OptionType.Numeric:
                    var minValue = param[0];
                    var maxValue = param[1];
                    Options = Enumerable.Range(minValue, maxValue - minValue + 1).Select(e => e.ToString()).ToList();
                    break;
            }
            Options.Add(" ");
        }

        public enum OptionType
        {
            DateYear,
            Severity,
            Numeric
        }
    }

    /// <summary>
    /// To retrieve attributes of a PropertyInfo object
    /// </summary>
    public static class AttributeHelper
    {
        /// <summary>
        /// Use reflection to obtain the display name for a given attribute
        /// </summary>
        public static string GetDisplayName(PropertyInfo prop)
        {
            var displayAttribute = prop.GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;
            return displayAttribute != null ? displayAttribute.Name : prop.Name;
        }

        /// <summary>
        /// Use reflection to obtain the options of a property
        /// </summary>
        public static IEnumerable<string> GetOptions(PropertyInfo prop)
        {
            var option = prop.GetCustomAttribute(typeof(OptionsAttribute)) as OptionsAttribute;
            return option == null ? null : option.Options;
        }

        public static RangeAttribute GetRange(PropertyInfo prop)
        {
            var range = prop.GetCustomAttribute(typeof(RangeAttribute)) as RangeAttribute;
            return range == null ? null : range;
        }
    }

    public class RangeAttribute : Attribute
    {
        public double Min { get; set; }

        public double Max { get; set; }

        public RangeAttribute(RangeType type, double min = 0, double max = 100)
        {
            Min = min;
            Max = max;
        }

        public enum RangeType
        {
            Number,
            Percentage
        }
    }
}