﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetroCOPACESGrid.Utility;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace MetroCOPACESGrid.DataModel
{
    public class RoadSurvey
    {
        // Default values
        public RoadSurvey() { }
        public RoadSurvey(Road road)
        {
            Id = DateTime.Now.ToFileTime() + "-" + road.Id;
            IsFinished = false;
            Date = DateTime.Now;
            RoadId = road.Id;
            PlaceId = road.PlaceId;
        }

        [JsonProperty(PropertyName = "bleedAvg")]
        public double? BleedAvg { get; set; }

        [JsonProperty(PropertyName = "bleedDeduct")]
        public double? BleedDeduct { get; set; }

        [JsonProperty(PropertyName = "bleedLevel")]
        public double? BleedLevel { get; set; }

        [JsonProperty(PropertyName = "blockAvg")]
        public double? BlockAvg { get; set; }

        [JsonProperty(PropertyName = "blockDeduct")]
        public double? BlockDeduct { get; set; }

        [JsonProperty(PropertyName = "blockLevel")]
        public double? BlockLevel { get; set; }

        [JsonProperty(PropertyName = "corrugAvg")]
        public double? CorrugAvg { get; set; }

        [JsonProperty(PropertyName = "corrugDeduct")]
        public double? CorrugDeduct { get; set; }

        [JsonProperty(PropertyName = "corrugLevel")]
        public double? CorrugLevel { get; set; }

        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }

        [JsonProperty(PropertyName = "edgeAvg")]
        public double? EdgeAvg { get; set; }

        [JsonProperty(PropertyName = "edgeDeduct")]
        public double? EdgeDeduct { get; set; }

        [JsonProperty(PropertyName = "edgeLevel")]
        public double? EdgeLevel { get; set; }

        [JsonProperty(PropertyName = "finishDate")]
        public DateTime? FinishDate { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "isFinished")]
        public bool IsFinished { get; set; }

        [JsonProperty(PropertyName = "loadLevel1Avg")]
        public double? LoadLevel1Avg { get; set; }

        [JsonProperty(PropertyName = "loadLevel1Deduct")]
        public double? LoadLevel1Deduct { get; set; }

        [JsonProperty(PropertyName = "loadLevel2Avg")]
        public double? LoadLevel2Avg { get; set; }

        [JsonProperty(PropertyName = "loadLevel2Deduct")]
        public double? LoadLevel2Deduct { get; set; }

        [JsonProperty(PropertyName = "loadLevel3Avg")]
        public double? LoadLevel3Avg { get; set; }

        [JsonProperty(PropertyName = "loadLevel3Deduct")]
        public double? LoadLevel3Deduct { get; set; }

        [JsonProperty(PropertyName = "loadLevel4Avg")]
        public double? LoadLevel4Avg { get; set; }

        [JsonProperty(PropertyName = "loadLevel4Deduct")]
        public double? LoadLevel4Deduct { get; set; }

        [JsonProperty(PropertyName = "lossAvg")]
        public double? LossAvg { get; set; }

        [JsonProperty(PropertyName = "lossDeduct")]
        public double? LossDeduct { get; set; }

        [JsonProperty(PropertyName = "lossLevel")]
        public double? LossLevel { get; set; }

        [JsonProperty(PropertyName = "patchAvg")]
        public double? PatchAvg { get; set; }

        [JsonProperty(PropertyName = "patchDeduct")]
        public double? PatchDeduct { get; set; }

        [JsonProperty(PropertyName = "placeId")]
        public String PlaceId { get; set; }

        [JsonProperty(PropertyName = "projectRating")]
        public double? ProjectRating { get; set; }

        [JsonProperty(PropertyName = "ravelAvg")]
        public double? RavelAvg { get; set; }

        [JsonProperty(PropertyName = "ravelDeduct")]
        public double? RavelDeduct { get; set; }

        [JsonProperty(PropertyName = "ravelLevel")]
        public double? RavelLevel { get; set; }

        [JsonProperty(PropertyName = "reflectAvg")]
        public double? ReflectAvg { get; set; }

        [JsonProperty(PropertyName = "reflectDeduct")]
        public double? ReflectDeduct { get; set; }

        [JsonProperty(PropertyName = "reflectLevel")]
        public double? ReflectLevel { get; set; }

        [JsonProperty(PropertyName = "roadId")]
        public String RoadId { get; set; }

        [JsonProperty(PropertyName = "rutAvg")]
        public double? RutAvg { get; set; }

        [JsonProperty(PropertyName = "rutDeduct")]
        public double? RutDeduct { get; set; }

        [JsonProperty(PropertyName = "slopeAvg")]
        public double? SlopeAvg { get; set; }

        [JsonProperty(PropertyName = "slopeDeduct")]
        public double? SlopeDeduct { get; set; }

        [Version]
        public String Version { get; set; }

        /// <summary>
        /// Find segment surveys belonging to this road survey
        /// </summary>
        public async Task<List<int>> GetSurveyedSegmentInventIds()
        {
            var res = await App.MobileService.GetSyncTable<SegmentSurvey>()
                .Where(ss => ss.RoadSurveyId == this.Id)
                .Select(ss => ss.InventId).ToEnumerableAsync();
            return res.Distinct().ToList() ?? null;
        }

        public async Task<List<Segment>> GetSurveyedSegments()
        {
            var ids = await GetSurveyedSegmentInventIds();
            return await SyncHelper.GetSegmentsByIds(ids);
        }

        public async Task<List<SegmentSurvey>> GetSegmentSurveys()
        {
            return await SyncHelper.GetSegmentSurveysByRoadSurvey(this);
        }

        /// <summary>
        /// Find segment surveys belonging to this road survey
        /// </summary>
        public async Task<List<int>> GetUnSurveyedSegmentInventIds()
        {
            var allSegmentIds = await App.LocalSegmentTable
                .Where(s => s.RoadId == RoadId)
                .Select(s => s.InventId)
                .ToListAsync();
            var surveyedSegmentIds = await GetSurveyedSegmentInventIds();
            return allSegmentIds.Where(e => allSegmentIds.Contains(e) && !surveyedSegmentIds.Contains(e)).ToList();
        }

        public async Task CheckIfFinished()
        {
            var unSurveyed = await GetUnSurveyedSegmentInventIds();
            if (unSurveyed != null && unSurveyed.Count > 0)
                return;

            IsFinished = true;
            FinishDate = DateTime.Now;
            await App.LocalRoadSurveyTable.UpdateAsync(this);

            var road = await App.LocalRoadTable.LookupAsync(RoadId);
            await road.CheckIfHasUnfinishedSurvey();
        }
    }
}