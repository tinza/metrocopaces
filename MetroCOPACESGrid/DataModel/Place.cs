﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace MetroCOPACESGrid.DataModel
{
    public class Place
    {
        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public String Name { get; set; }

        [JsonProperty(PropertyName = "type")]
        public String Type { get; set; }

        [JsonProperty(PropertyName = "countyNo")]
        public int? CountyNo { get; set; }

        [JsonProperty(PropertyName = "district")]
        public int? District { get; set; }

        [Version]
        public string Version { get; set; }
        
        public override string ToString()
        {
            return Name;
        }
    }
}
