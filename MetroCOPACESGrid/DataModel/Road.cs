﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MetroCOPACESGrid.Utility;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace MetroCOPACESGrid.DataModel
{
    public class Road
    {
        [Display(Name = "Bridge Number")]
        [JsonProperty(PropertyName = "bridgeNum")]
        public int? BridgeNum { get; set; }

        [Display(Name = "Bridge Width")]
        [JsonProperty(PropertyName = "bridgeWidth")]
        public double? BridgeWidth { get; set; }

        [Display(Name = "Culvert And Pipe Number")]
        [JsonProperty(PropertyName = "culvertAndPipeNum")]
        public int? CulvertAndPipeNum { get; set; }

        [JsonProperty(PropertyName = "district")]
        public String District { get; set; }

        [JsonProperty(PropertyName = "from")]
        public String From { get; set; }

        [Display(Name = "Functional Class")]
        [JsonProperty(PropertyName = "functionalClass")]
        public String FunctionalClass { get; set; }

        [JsonProperty(PropertyName = "hasUnfinishedSurvey")]
        public bool HasUnfinishedSurvey { get; set; }

        [ReadOnly]
        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "jurisdiction")]
        public String Jurisdiction { get; set; }

        [Display(Name = "Lane Number")]
        [JsonProperty(PropertyName = "laneNum")]
        public int? LaneNum { get; set; }

        [JsonProperty(PropertyName = "lastSurveyDate")]
        [Display(Name = "Last Survey")]
        public DateTime? LastSurveyDate { get; set; }

        [JsonProperty(PropertyName = "name")]
        public String Name { get; set; }

        //Todo: Change to the right options
        [Display(Name = "Pavment Marking Condition")]
        [Options("Good", "Bad")]
        [JsonProperty(PropertyName = "pavMarkingCondition")]
        public String PavMarkingCondition { get; set; }

        [Display(Name = "Maximum Pavement Width")]
        [JsonProperty(PropertyName = "pavWidthMax")]
        public double? PavWidthMax { get; set; }

        [Display(Name = "Minimum Pavement Width")]
        [JsonProperty(PropertyName = "pavWidthMin")]
        public double? PavWidthMin { get; set; }

        [Display(Name = "Typical Pavement Width")]
        [JsonProperty(PropertyName = "pavWidthTypical")]
        public double? PavWidthTypical { get; set; }

        [Internal]
        [JsonProperty(PropertyName = "placeId")]
        public String PlaceId { get; set; }

        [Internal]
        [JsonProperty(PropertyName = "placeType")]
        public String PlaceType { get; set; }

        [JsonProperty(PropertyName = "remarks")]
        public String Remarks { get; set; }

        [Display(Name = "Maximum Shoulder Width")]
        [JsonProperty(PropertyName = "shoulderWidthMax")]
        public double? ShoulderWidthMax { get; set; }

        [Display(Name = "Minimum Shoulder Width")]
        [JsonProperty(PropertyName = "shoulderWidthMin")]
        public double? ShoulderWidthMin { get; set; }

        [Display(Name = "Typical Shoulder Width")]
        [JsonProperty(PropertyName = "shoulderWidthTypical")]
        public double? ShoulderWidthTypical { get; set; }

        [Display(Name = "Surface Type")]
        [Options("Concrete", "Asphalt")]
        [JsonProperty(PropertyName = "surfaceType")]
        public String SurfaceType { get; set; }

        [JsonProperty(PropertyName = "to")]
        public String To { get; set; }

        [Display(Name = "Unpaved Shoulder Width")]
        [JsonProperty(PropertyName = "unpavedShoulderWidth")]
        public double? UnpavedShoulderWidth { get; set; }

        [Version]
        public string Version { get; set; }

        public async Task<List<Segment>> GetSegments()
        {
            return await SyncHelper.GetSegmentsByRoadId(Id);
        }

        public async Task<RoadSurvey> GetLatestSurvey()
        {
            var surveys = await SyncHelper.GetRoadSurveysByRoadId(Id);
            return surveys.FirstOrDefault();
        }
        /// <summary>
        /// To check if the road has any unfinished RoadSurvey
        /// </summary>
        public async Task CheckIfHasUnfinishedSurvey()
        {
            if (!(await App.LocalRoadSurveyTable
                .Where(rs => rs.RoadId == Id && rs.IsFinished == false)
                .ToListAsync()).Any())
            {
                HasUnfinishedSurvey = false;
                await App.LocalRoadTable.UpdateAsync(this);
            }
        }

        public async Task AddNewSurvey(RoadSurvey roadSurvey)
        {
            LastSurveyDate = roadSurvey.Date;
            HasUnfinishedSurvey = true;
            await App.LocalRoadTable.UpdateAsync(this);
        }
    }
}