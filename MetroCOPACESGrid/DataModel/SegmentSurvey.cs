﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MetroCOPACESGrid.Utility;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace MetroCOPACESGrid.DataModel
{
    public class SegmentSurvey : INotifyPropertyChanged
    {
        // Default values
        public SegmentSurvey(RoadSurvey roadSurvey, Segment segment)
        {
            IsWindshieldSurvey = false;
            SegSurveyDate = DateTime.Now;

            RoadSurveyId = roadSurvey.Id;
            RoadSurveyDate = roadSurvey.Date;
            RoadId = roadSurvey.RoadId;

            InventId = segment.InventId;
            PlaceId = segment.PlaceId;
            SequenceId = segment.SequenceId;

            Id = ((DateTime) SegSurveyDate).ToFileTime() + "-" + InventId;

            CopacesRating = 100;
        }

        public SegmentSurvey()
        {
        }

        [JsonProperty(PropertyName = "bleedLevel")]
        [Display(Name = "Severity")]
        [Options(OptionsAttribute.OptionType.Severity, 3)]
        public int? BleedLevel { get; set; }

        [JsonProperty(PropertyName = "bleedPercentage")]
        [Display(Name = "Bleed Percentage")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? BleedPercentage { get; set; }

        [JsonProperty(PropertyName = "blockLevel")]
        [Display(Name = "Severity")]
        [Options(OptionsAttribute.OptionType.Severity, 3)]
        public int? BlockLevel { get; set; }

        [JsonProperty(PropertyName = "blockPercentage")]
        [Display(Name = "Block Percentage")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? BlockPercentage { get; set; }

        private double? _copacesRating;
        [JsonProperty(PropertyName = "copacesRating")]
        [Display(Name = "COPACES Rating")]
        public double? CopacesRating
        {
            get { return _copacesRating; }
            set
            {
                _copacesRating = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty(PropertyName = "corrugLevel")]
        [Display(Name = "Severity")]
        [Options(OptionsAttribute.OptionType.Severity, 3)]
        public int? CorrugLevel { get; set; }

        [JsonProperty(PropertyName = "corrugPercentage")]
        [Display(Name = "Corrug Percentage")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? CorrugPercentage { get; set; }

        [JsonProperty(PropertyName = "crossSlopeLeft")]
        [Display(Name = "Cross Slope(L)")]
        public double? CrossSlopeLeft { get; set; }

        [JsonProperty(PropertyName = "crossSlopeRight")]
        [Display(Name = "Cross Slope(R)")]
        public double? CrossSlopeRight { get; set; }

        [JsonProperty(PropertyName = "edgeLevel")]
        [Display(Name = "Severity")]
        [Options(OptionsAttribute.OptionType.Severity, 3)]
        public int? EdgeLevel { get; set; }

        [JsonProperty(PropertyName = "edgePercentage")]
        [Display(Name = "Edge Percentage")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? EdgePercentage { get; set; }

        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "inventId")]
        [Display(Name = "Invent Id")]
        public int InventId { get; set; }

        private bool _isWindshieldSurvey;
        [JsonProperty(PropertyName = "isWindshieldSurvey")]
        public bool IsWindshieldSurvey
        {
            get { return _isWindshieldSurvey; }
            set
            {
                _isWindshieldSurvey = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty(PropertyName = "laneDirection")]
        [Options("North", "South", "East", "West", "NorthEast", "NorthWest", "SouthEast", "SouthWest")]
        public String LaneDirection { get; set; }

        [JsonProperty(PropertyName = "laneNum")]
        [Display(Name = "Lane Number")]
        [Options(OptionsAttribute.OptionType.Numeric, 1, 10)]
        public String LaneNum { get; set; }

        [JsonProperty(PropertyName = "loadLevel1")]
        [Display(Name = "Load Level 1")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? LoadLevel1 { get; set; }

        [JsonProperty(PropertyName = "loadLevel2")]
        [Display(Name = "Load Level 2")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? LoadLevel2 { get; set; }

        [JsonProperty(PropertyName = "loadLevel3")]
        [Display(Name = "Load Level 3")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? LoadLevel3 { get; set; }

        [JsonProperty(PropertyName = "loadLevel4")]
        [Display(Name = "Load Level 4")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? LoadLevel4 { get; set; }

        [JsonProperty(PropertyName = "lossPavLevel")]
        [Display(Name = "Severity")]
        [Options(OptionsAttribute.OptionType.Severity, 3)]
        public int? LossPavLevel { get; set; }

        [JsonProperty(PropertyName = "lossPavPercentage")]
        [Display(Name = "Loss Pav Percentage")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? LossPavPercentage { get; set; }

        [JsonProperty(PropertyName = "patchPotholeNum")]
        [Display(Name = "Patch/Pothole Num")]
        public int? PatchPotholeNum { get; set; }

        [JsonProperty(PropertyName = "placeId")]
        public String PlaceId { get; set; }

        [JsonProperty(PropertyName = "rater")]
        public String Rater { get; set; }

        [JsonProperty(PropertyName = "ravelLevel")]
        [Display(Name = "Severity")]
        [Options(OptionsAttribute.OptionType.Severity, 3)]
        public int? RavelLevel { get; set; }

        [JsonProperty(PropertyName = "ravelPercentage")]
        [Display(Name = "Ravel Percentage")]
        [Range(RangeAttribute.RangeType.Percentage)]
        public double? RavelPercentage { get; set; }

        [JsonProperty(PropertyName = "reflectLength")]
        [Display(Name = "Reflect Length")]
        public double? ReflectLength { get; set; }

        [JsonProperty(PropertyName = "reflectLevel")]
        [Display(Name = "Severity")]
        [Options(OptionsAttribute.OptionType.Severity, 3)]
        public int? ReflectLevel { get; set; }

        [JsonProperty(PropertyName = "reflectNum")]
        [Display(Name = "Relfect Number")]
        public double? ReflectNum { get; set; }

        [JsonProperty(PropertyName = "remarks")]
        public String Remarks { get; set; }

        [JsonProperty(PropertyName = "roadId")]
        [Display(Name = "Road Id")]
        public String RoadId { get; set; }

        [JsonProperty(PropertyName = "roadSurveyDate")]
        [Display(Name = "Road Survey Date")]
        public DateTime? RoadSurveyDate { get; set; }

        [JsonProperty(PropertyName = "roadSurveyId")]
        public String RoadSurveyId { get; set; }

        [JsonProperty(PropertyName = "rutInWp")]
        [Display(Name = "Rut In WP")]
        public double? RutInWp { get; set; }

        [JsonProperty(PropertyName = "rutOutWp")]
        [Display(Name = "Rut Out WP")]
        public double? RutOutWp { get; set; }

        [JsonProperty(PropertyName = "sampleLocation")]
        [Display(Name = "Sample Location")]
        public String SampleLocation { get; set; }

        [JsonProperty(PropertyName = "segSurveyDate")]
        [Display(Name = "Segment Survey Date")]
        public DateTime? SegSurveyDate { get; set; }

        [JsonProperty(PropertyName = "sequenceId")]
        [Display(Name = "Sequence Id")]
        public int? SequenceId { get; set; }

        [JsonProperty(PropertyName = "treatmentMethod")]
        [Options("Chip Seal", "Reconstructed", "Resurfaced")]
        [Display(Name = "Treatment Method")]
        public String TreatmentMethod { get; set; }

        [JsonProperty(PropertyName = "treatmentYear")]
        [Display(Name = "Treatment Year")]
        [Options(OptionsAttribute.OptionType.DateYear, 20)]
        public String TreatmentYear { get; set; }

        [Version]
        public string Version { get; set; }

        [JsonProperty(PropertyName = "windshieldScore")]
        [Range(RangeAttribute.RangeType.Number, 0, 100)]
        public double? WindshieldScore { get; set; }

        public void CalculateRating()
        {
            try
            {
                Func<double?, int> nullConverter = (double? x) => x == null ? 0 : Convert.ToInt32(x);
                var segmentArray = new[]
                {
                    nullConverter(RutOutWp),
                    nullConverter(RutInWp),
                    nullConverter(LoadLevel1),
                    nullConverter(LoadLevel2),
                    nullConverter(LoadLevel3),
                    nullConverter(LoadLevel4),
                    nullConverter(BlockPercentage),
                    nullConverter(BlockLevel),
                    nullConverter(ReflectNum),
                    nullConverter(ReflectLength),
                    nullConverter(ReflectLevel),
                    nullConverter(RavelPercentage),
                    nullConverter(RavelLevel),
                    nullConverter(EdgePercentage),
                    nullConverter(EdgeLevel),
                    nullConverter(BleedPercentage),
                    nullConverter(BleedLevel),
                    nullConverter(CorrugPercentage),
                    nullConverter(CorrugLevel),
                    nullConverter(LossPavPercentage),
                    nullConverter(LossPavLevel),
                    nullConverter(CrossSlopeLeft),
                    nullConverter(CrossSlopeRight),
                    PatchPotholeNum ?? 0
                };
                var copacesSegment = new GDOTPACESLib.Segment(segmentArray);
                CopacesRating = copacesSegment.SegmentRating;
            }
            catch (Exception)
            {
                return;
            }
        }

        public async Task SaveToDb()
        {
            if (CopacesRating == null)
                CalculateRating();

            await App.LocalSegmentSurveyTable.InsertAsync(this);

            // Check if the associated RoadSurvey is finished
            var roadSurvey = await App.LocalRoadSurveyTable.LookupAsync(RoadSurveyId);
            await roadSurvey.CheckIfFinished();
        }

        public async Task<Segment> GetSegment()
        {
            return InventId != null
                ? await SyncHelper.GetSegmentByInventId((int)InventId)
                : null;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}