﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using MetroCOPACESGrid.Common;
using MetroCOPACESGrid.DataModel;
using MetroCOPACESGrid.Utility;

namespace MetroCOPACESGrid
{
    public class RoadSelectionPageViewModel : ObservableDictionary
    {
        // All the roads that the place has
        public IEnumerable<Road> AllRoads
        {
            get { return ContainsKey("AllRoads") ? this["AllRoads"] as IEnumerable<Road> : null; }
            set { this["AllRoads"] = value; }
        }

        // Current selected road
        public Road CurRoad
        {
            get { return this["CurRoad"] as Road; } set { this["CurRoad"] = value; } 
        }

        // The filtered road list
        public IEnumerable<Road> Roads
        {
            get { return this["Roads"] as IEnumerable<Road>; }
            set
            {
                this["Roads"] = value;
                CurRoad = value.FirstOrDefault();
            }
        }

        // The segments that the selceted road has
        public List<Segment> Segments
        {
            get { return this["Segments"] as List<Segment>; }
            set { this["Segments"] = value; }
        }
    }

    /// <summary>
    ///     A page that displays a group title, a list of items within the group, and details for
    ///     the currently selected item.
    /// </summary>
    public sealed partial class RoadSelectionPage : Page
    {
        private RoadSelectionPageViewModel defaultViewModel = new RoadSelectionPageViewModel();
        private NavigationHelper navigationHelper;

        public RoadSelectionPage()
        {
            this.InitializeComponent();

            // Setup the navigation helper
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            this.NavigationCacheMode = NavigationCacheMode.Enabled;

            // Setup the logical page navigation components that allow
            // the page to only show one pane at a time.
            this.navigationHelper.GoBackCommand = new RelayCommand(() => this.GoBack(), () => this.CanGoBack());

            // Start listening for Window size changes 
            // to change from showing two panes to showing a single pane
            Window.Current.SizeChanged += Window_SizeChanged;
            this.InvalidateVisualState();
        }

        public NavigationHelper NavigationHelper { get { return this.navigationHelper; } }

        public RoadSelectionPageViewModel ViewModel { get { return defaultViewModel; } }

        /// <summary>
        /// "Continue Last Survey" Callback
        /// </summary>
        private async void ButtonContinueSurvey_OnClick(object sender, RoutedEventArgs e)
        {
            var road = ViewModel.CurRoad;
            var roadSurvey = await SyncHelper.GetLatestRoadSurveyByRoadId(road.Id);

            if (roadSurvey == null)
            {
                road.HasUnfinishedSurvey = false;
                await App.LocalRoadTable.UpdateAsync(road);
                return;
            }

            var confirmDialog = new MessageDialog(
                String.Format(
                    "This will continue last unfinished road survey for road: {0} created at {1}, are you sure?",
                    road.Name, ((DateTime)roadSurvey.Date).ToString("d"))
                );
            confirmDialog.Commands.Add(new UICommand("Confirm"));
            confirmDialog.Commands.Add(new UICommand("Cancel"));
            var confirmRes = await confirmDialog.ShowAsync();

            if (confirmRes.Label == "Confirm")
            {
                var surveyedSegmentIds = await roadSurvey.GetSurveyedSegmentInventIds();
                var unSurveyedSegments = ViewModel.Segments
                    .Where(s => !surveyedSegmentIds.Contains(s.InventId))
                    .ToList();

                // If all segments are surveyed
                if (unSurveyedSegments.Count == 0)
                {
                    await roadSurvey.CheckIfFinished();
                    await new MessageDialog("This survey is already finished.").ShowAsync();
                    SearchBox_OnQuerySubmitted(SearchBox, null);
                    return;
                }

                Frame.Navigate(typeof(NewSurveyPage), new NewSurveyPageNavParameter
                {
                    Segments = unSurveyedSegments,
                    RoadSurvey = roadSurvey
                });
            }
        }

        private async void ButtonEditSurvey_OnClick(object sender, RoutedEventArgs e)
        {
            var road = roadListView.SelectedItem as Road;
            if (road == null) return;

            if (road.LastSurveyDate == null)
            {
                new MessageDialog("This road has never been surveyed.").ShowAsync();
                return;
            }

            var curRoadSurvey = await SyncHelper.GetLatestRoadSurveyByRoadId(road.Id);
            var curSegmentSurveys = await SyncHelper.GetSegmentSurveysByRoadSurvey(curRoadSurvey);
            if (curSegmentSurveys.Count == 0)
            {
                new MessageDialog("This road survey has started but no segment survey has been added yet.").ShowAsync();
                return;
            }

            Frame.Navigate(typeof(EditSurveyPage), new EditSurveyPageNavParameter
            {
                RoadSurvey = curRoadSurvey,
                SegmentSurveys = curSegmentSurveys
            });
        }

        /// <summary>
        ///     "Start New Survey" Callback
        /// </summary>
        private async void ButtonStartSurvey_OnClick(object sender, RoutedEventArgs e)
        {
            var road = ViewModel.CurRoad;

            var confirmDialog =
                new MessageDialog("This will create a new road survey for road: " + road.Name + ", are you sure?");
            confirmDialog.Commands.Add(new UICommand("Confirm"));
            confirmDialog.Commands.Add(new UICommand("Cancel"));
            var confirmRes = await confirmDialog.ShowAsync();

            if (confirmRes.Label == "Confirm")
            {
                // Create a new road survey
                var newRoadSurvey = new RoadSurvey(road);
                await SyncHelper.AddNewRoadSurvey(newRoadSurvey, road);

                Frame.Navigate(typeof(NewSurveyPage),
                    new NewSurveyPageNavParameter
                    {
                        Segments = ViewModel.Segments,
                        RoadSurvey = newRoadSurvey
                    });
            }
        }

        /// <summary>
        ///     Callback of the "Edit" flyout button for road/segment list
        /// </summary>
        private void ListItemFlyout_Click(object sender, RoutedEventArgs e)
        {
            var item = ((FrameworkElement)sender).DataContext;
            if (item != null)
            {
                Frame.Navigate(typeof(EditRoadSegmentPage), item);
            }
        }

        /// <summary>
        ///     Invoked when the user press and hold on the road/segment listview
        /// </summary>
        private void ListViewItem_Holding(object sender, HoldingRoutedEventArgs e)
        {
            var senderElement = sender as FrameworkElement;
            var flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);
            flyoutBase.ShowAt(senderElement);
        }

        /// <summary>
        ///     Used to create states when the page is first loaded
        /// </summary>
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            if (e.PageState == null)
            {
                ViewModel.AllRoads = await SyncHelper.GetLocalRoads();
                ViewModel.Roads = ViewModel.AllRoads;
                if (ViewModel.AllRoads == null || ViewModel.AllRoads.FirstOrDefault() == null)
                {
                    NotificationHelper.ShowErrorMessage(
                        "Cannot found local copy of the road data. Please download the data by click the 'Sync Data' button");

                    ButtonStartNewSurvey.IsEnabled = false;
                    ButtonContinueLastSurvey.IsEnabled = false;
                    ButtonEditSurvey.IsEnabled = false;
                    return;
                }

                if (!UsingLogicalPageNavigation() && RoadsViewSource.View != null)
                {
                    RoadsViewSource.View.MoveCurrentToFirst();
                }
            }
            else
            {
                SearchBox.QueryText = (string)e.PageState["QueryText"];
                RoadListUnfinishedFilterCheckBox.IsChecked = (bool)e.PageState["UnfinishedFilterState"];
                RoadListDateFilterCheckBox.IsChecked = (bool)e.PageState["DateFilterState"];
                if (RoadListDateFilterCheckBox.IsChecked == true)
                    RoadListDateFilterDatePicker.Date = (DateTimeOffset)e.PageState["DateFilterData"];

                if (String.IsNullOrEmpty(SearchBox.QueryText) &&
                    RoadListUnfinishedFilterCheckBox.IsChecked == false &&
                    RoadListDateFilterCheckBox.IsChecked == false)
                {
                    ViewModel.AllRoads = await SyncHelper.GetLocalRoads();
                    ViewModel.Roads = ViewModel.AllRoads;
                }
                else
                {
                    SearchBox_OnQuerySubmitted(SearchBox, null);
                }
            }
        }

        /// <summary>
        ///     Used to save states when leaving from the page
        /// </summary>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (this.RoadsViewSource.View == null) return;

            e.PageState["QueryText"] = SearchBox.QueryText;
            e.PageState["UnfinishedFilterState"] = RoadListUnfinishedFilterCheckBox.IsChecked;
            e.PageState["DateFilterState"] = RoadListDateFilterCheckBox.IsChecked;
            e.PageState["DateFilterData"] = RoadListDateFilterDatePicker.Date;
            e.PageState["AllRoads"] = ViewModel.AllRoads;
        }

        /// <summary>
        ///     Invoked when an road is selected.
        /// </summary>
        private async void RoadList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var road = ((ListView)sender).SelectedItem as Road;

            if (road != null)
            {
                ViewModel.Segments = await SyncHelper.GetSegmentsByRoadId(road.Id);

                // Toggle the edit button
                if (road.LastSurveyDate == null)
                    ButtonEditSurvey.IsEnabled = false;
                else
                {
                    var curRoadSurvey = await SyncHelper.GetLatestRoadSurveyByRoadId(road.Id);
                    var curSegmentSurveys = await SyncHelper.GetSegmentSurveysByRoadSurvey(curRoadSurvey);
                    ButtonEditSurvey.IsEnabled = curSegmentSurveys.Count != 0;
                }

                // Toggle the start new survey button / continue survey button
                ButtonStartNewSurvey.IsEnabled = !road.HasUnfinishedSurvey;
                ButtonContinueLastSurvey.IsEnabled = road.HasUnfinishedSurvey;
            }
            else
            {
                ButtonStartNewSurvey.IsEnabled = false;
                ButtonContinueLastSurvey.IsEnabled = false;
                ButtonEditSurvey.IsEnabled = false;
            }

            if (UsingLogicalPageNavigation()) InvalidateVisualState();
        }

        /// <summary>
        ///     Invoked when the user finished typing in the search box
        /// </summary>
        private async void SearchBox_OnQuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            var queryText = sender.QueryText;
            var hasUnfinishedSurveyFilter = RoadListUnfinishedFilterCheckBox.IsChecked == true;
            DateTime? lastSurveyDateFilter = null;
            if (RoadListDateFilterCheckBox.IsChecked == true)
                lastSurveyDateFilter = RoadListDateFilterDatePicker.Date.DateTime;
            ViewModel["Roads"] = await SyncHelper.SearchRoadsByName(queryText, hasUnfinishedSurveyFilter, lastSurveyDateFilter);
        }

        #region Logical page navigation

        // The split page isdesigned so that when the Window does have enough space to show
        // both the list and the dteails, only one pane will be shown at at time.
        //
        // This is all implemented with a single physical page that can represent two logical
        // pages.  The code below achieves this goal without making the user aware of the
        // distinction.

        private const int MinimumWidthForSupportingTwoPanes = 768;

        /// <summary>
        ///     Invoked to determine whether the page should act as one logical page or two.
        /// </summary>
        /// <returns>
        ///     True if the window should show act as one logical page, false
        ///     otherwise.
        /// </returns>
        private bool UsingLogicalPageNavigation()
        {
            return Window.Current.Bounds.Width < MinimumWidthForSupportingTwoPanes;
        }

        /// <summary>
        ///     Invoked with the Window changes size
        /// </summary>
        /// <param name="sender">The current Window</param>
        /// <param name="e">Event data that describes the new size of the Window</param>
        private void Window_SizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            this.InvalidateVisualState();
        }

        private bool CanGoBack()
        {
            if (this.UsingLogicalPageNavigation() && this.roadListView.SelectedItem != null)
            {
                return true;
            }
            else
            {
                return this.navigationHelper.CanGoBack();
            }
        }

        private void GoBack()
        {
            if (this.UsingLogicalPageNavigation() && this.roadListView.SelectedItem != null)
            {
                // When logical page navigation is in effect and there's a selected item that
                // item's details are currently displayed.  Clearing the selection will return to
                // the item list.  From the user's point of view this is a logical backward
                // navigation.
                this.roadListView.SelectedItem = null;
            }
            else
            {
                this.navigationHelper.GoBack();
            }
        }

        private void InvalidateVisualState()
        {
            var visualState = DetermineVisualState();
            VisualStateManager.GoToState(this, visualState, false);
            this.navigationHelper.GoBackCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        ///     Invoked to determine the name of the visual state that corresponds to an application
        ///     view state.
        /// </summary>
        /// <returns>
        ///     The name of the desired visual state.  This is the same as the name of the
        ///     view state except when there is a selected item in portrait and snapped views where
        ///     this additional logical page is represented by adding a suffix of _Detail.
        /// </returns>
        private string DetermineVisualState()
        {
            if (!UsingLogicalPageNavigation())
                return "PrimaryView";

            // Update the back button's enabled state when the view state changes
            var logicalPageBack = this.UsingLogicalPageNavigation() && this.roadListView.SelectedItem != null;

            return logicalPageBack ? "SinglePane_Detail" : "SinglePane";
        }

        #endregion

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the
        /// <see cref="GridCS.Common.NavigationHelper.LoadState" />
        /// and
        /// <see cref="GridCS.Common.NavigationHelper.SaveState" />
        /// .
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}